# JOptimFramework #

This framework provides a convenient, object-oriented representation of components needed to model optimization problems, especially linear programs (LP) and systems of difference constraints (SDC). It is completely written in the Java programming language and released under Apache 2.0-License. 

I originally created the framework for my bachelor-thesis at the chair of Embedded Systems and their Applications (ESA) at 
Technische Universität Darmstadt (Germany). My thesis was supervised by Prof. Dr.-Ing. Andreas Koch and Dipl.-Inform. Julian 
Oppermann.

To obtain a version of the framework you can either use a git clone, which includes a eclipse-project file, or you can download the provided zip-file in the download-section. 

## Features ##

The essential feature of this framework is a convenient object-representation for two constructs: Linear programs (LP) and systems of difference constraints (SDC).

A system of difference constraints consists of multiple difference constraints of the form: x - y =< c, where x and y are variables and c is a constant. One major task performed on SDC is feasibility checking, which tests whether there is a variable assignment fullfilling all constraints or not. This can be done efficiently with graph-based algorithms. The framework provides an object-representation for all components, e.g. variables or constraints, and multiple algorithm-implementations for feasibility checking. 

Linear programming is a standard technique in operations research. A linear program consists of multiple linear constraints, variable constraints and a linear objective function. The framework provides supplies object-representation for all this components. 
The framework does not implement its own LP solver, but instead provides connectors to different open-source LP-solvers, e.g.:

* lp_solve (http://lpsolve.sourceforge.net/5.5/)

* Apache SimplexSolver from Apache Commons Math (http://commons.apache.org/proper/commons-math/javadocs/api-3.3/org/apache/commons/math3/optim/linear/SimplexSolver.html)

* WVLPSolver (http://www.win-vector.com/blog/2012/11/yet-another-java-linear-programming-library/)

The LPs can automatically be translated from the frameworks own representation to the solvers specific representation, so there is no need for you to write any calls to the solver's API. This gives you the option to test and use multiple different solvers without having to change a single line of your model's code.
Additionally a dumper to the CPLEX file format is existent, the resulting file can be used as input for many exisiting LP-solving tools. 

Besides these main features and its components there is a number of handy classes, mainly for debugging or printing. 
For some of the components unit/regression-tests are provided. 

## License ##
The framework is licensed under the Apache 2.0-License, a permissive, liberal open-source license. 
The various dependencies are also licensed under different open-source licenses, more detailed information on the license of each dependency can be obtained from the NOTICE-file. 

## Download and Installation ##

To obtain a version of the framework you can either use a git clone, which includes a eclipse-project file, or you can download the provided zip-file in the download-section. 
To build the framework from source the provided ANT build-file can be used. 

The provided release-file contains, next to licensing information, 3 main folders: 

* The **dist/**-folder contains the framework's jar and all dependencies. Next to that, a sources- and a javadoc-jar are provided for use in your IDE, e.g. Eclipse. 

* In the **lp_solve_lib/**-folder all header-files necessary for the set-up of lp_solve (described below) are included. 

* The framework's JAVADOC resides in the **javadoc/**-folder

The framework is not intended to be used as a stand-alone program, but should be used as referenced library. You just need to include it and its dependencies in your project's build path. It is important that the **lib/**-folder holding all dependencies and included in the **dist/**-directory always resides in the same folder as the **JOptimFramework**-jar, as this is mandatory for compiling and running the framework. 

### lp_solve
If you want to use lp_solve as LP-solver, additional setup is required. The shared-object files in the folder lp_solve_lib need to be in some directory that is on your system's library path **and** on the java library-path. You can check your setup for correctness with a call to JOptimFrameworkSetupChecker.testJOptimFrameworkSetup(). Additional information on the set-up for lp_solve can be found here: http://lpsolve.sourceforge.net/5.5/Java/README.html

#### Mac OS X
The precompiled lp_solve libraries are not compatible with recent versions of Mac OS X, so you have to build them yourself. Install lp_solve via Homebrew or build it from source, and use the supplied script **build-bindings-osx.sh** to generate **liblpsolve55j.dylib**

If you need the optional CPLEX export, get **lp_solve_5.5.2.0_source.tar.gz** and **lp_solve_5.5.2.0_xli_CPLEX_source.tar.gz** from http://sourceforge.net/projects/lpsolve/files and unpack them into the same directory. Build the plugin with `cd xli/xli_CPLEX ; sh ccc.osx`. This generates **xli/xli_CPLEX/bin/osx64/libxli_CPLEX.so**. Change the extension to **.dylib**.

Make sure that **liblpsolve55j.dylib** and **libxli_CPLEX.dylib** are in directories that are on the Java library path.

### CPLEX
If you want to use IBM ILOG CPLEX as the LP-solver, use the included file **build.properties.example** as a template to create a **build.properties** file in the project root, and adapt the property **cplex.home** to to your setup. The property **cplex.arch** has to be equal to the name of the subdirectory in **${cplex.home}/cplex/bin/**, e.g. 'x86-64_linux' for a 64-bit Linux system or 'x86-64_osx' for a OS X system.

## Use ##

To use the framework you model your problem with the different components. When you have done so, you can apply a solver or feasibility checker to it. 

Example-code for the construction of LP and SDC using the framework is provided in the examples-package. You can run these examples by adding "LPExample" or "SDCExample" as a parameter to a call to the framework's jar on the command-line.

For information on the construction and interfaces of the components, refer to the detailed JAVADOC-documentation included. 

The framework also includes debugging-facilities, some of them are capable of printing graphs in the popular dot-format. 

## Contribution ##

If you add additional features to the framework or fix a bug in it, please file a pull request or a ticket.

## CI-Status ##
[![Run Status](https://api.shippable.com/projects/55610dc2edd7f2c052f9deb7/badge?branch=master)](https://app.shippable.com/projects/55610dc2edd7f2c052f9deb7)