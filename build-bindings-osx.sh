#!/bin/sh
#
# Builds the native part of the lp_solve Java bindings.
#
# Change the paths below according to your setup.
#
# Based on the script "build-osx" from the lp_solve distribution.
#

JAVA_VERSION="1.8.0_65"
JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk$JAVA_VERSION.jdk/Contents/Home"
LP_SOLVE_INSTALL="/usr/local"

clang++ -fPIC -fno-common -Wno-unused-value\
	-I $JAVA_HOME/include -I $JAVA_HOME/include/darwin/ \
	-I $LP_SOLVE_INSTALL/include \
	-c lp_solve_lib/src/c/lpsolve5j.cpp

clang++ -dynamiclib -compatibility_version 5.5.0 -current_version 5.5.0 \
	-lc -L $LP_SOLVE_INSTALL/lib -llpsolve55 \
	-o liblpsolve55j.dylib \
	lpsolve5j.o

rm lpsolve5j.o

