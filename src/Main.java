/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import examples.LPExample;
import examples.SDCExample;
import test.framework.sdc.setup.JOptimFrameworkSetupChecker;


public class Main {
	
	public static void main(String[] args) {
		if(args.length > 0 && args[0] != null){
			if(args[0].equalsIgnoreCase("lpexample")){
				LPExample.main(new String[]{});
			}
			if(args[0].equalsIgnoreCase("sdcexample")){
				SDCExample.main(new String[]{});
			}
		}
		else{
			JOptimFrameworkSetupChecker.testJOptimFrameworkSetup();	
		}
		
		String s1 = "Elem1";
		String s2 = "Elem2";
		List<String> elements = new ArrayList<String>();
		elements.add(s1);
		elements.add(s2);
		final Map<String, Integer> schedule = new HashMap<String, Integer>();
		schedule.put(s1, 2);
		schedule.put(s2, 2);
		final Map<String, Integer> priority = new HashMap<String, Integer>();
		priority.put(s1, 5);
		priority.put(s2, 10);
		final Set<String> handled = new HashSet<String>();
		Comparator<String> test = new Comparator<String>(){

			@Override
			public int compare(String o1, String o2) {
				int schedSort = schedule.get(o1).compareTo(schedule.get(o2));
				if(schedSort == 0){
					if(handled.contains(o1) && !handled.contains(o2)){
						return -1;
					}
					if(handled.contains(o2) && !handled.contains(o1)){
						return 1;
					}
					return priority.get(o2).compareTo(priority.get(o1));
				}
				return schedSort;
			}
			
		};
		System.out.println(Integer.compare(2,2));
		Collections.sort(elements, test);
		System.out.println(elements);
	}
	

	

}
