/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.debug;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import framework.sdc.feasibility_checker.model.SDCFeasibilityGraphEdge;
import framework.sdc.feasibility_checker.model.util.Pair;
import framework.sdc.model.SDCVariable;

/**
 * Debbuging utils for the SDC.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class JOptimFrameworkDebugUtil {

	/**
	 * Print the feasibility graph to a dot-file.
	 * 
	 * @param filename File the graph is printed to
	 * @param edges All edges to be printed
	 * @param highlightLast If set to true, the edge added last is highlighted
	 *            with color
	 */
	public static void printFeasibilityGraph(String filename, List<SDCFeasibilityGraphEdge> edges, boolean highlightLast) {
		if (highlightLast) {
			edges.get(edges.size() - 1).setHighlighted(true);
		}
		File file = new File(filename);
		System.out.println("Printing graph to file: " + file.getAbsolutePath());
		try {
			FileOutputStream os = new FileOutputStream(file, false);
			PrintStream printer = new PrintStream(os);
			printer.println("digraph{");
			for (SDCFeasibilityGraphEdge edge : edges) {
				printer.println(edge.dotRepresentation());
			}
			printer.println("}");
			printer.close();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Print all edges included in a negative cycle to a dot-file
	 * 
	 * @param filename File to be printed to
	 * @param cycle Variables included in negative cycle
	 * @param edges All edges in the feasibility graph
	 */
	public static void printNegativeCycle(String filename, LinkedList<SDCVariable> cycle, Set<SDCFeasibilityGraphEdge> edges) {
		Map<Pair<SDCVariable, SDCVariable>, Double> cycleEdges = new HashMap<Pair<SDCVariable, SDCVariable>, Double>();
		for (int i = 1; i < cycle.size(); ++i) {
			SDCVariable start = cycle.get(i - 1);
			SDCVariable end = cycle.get(i);
			cycleEdges.put(new Pair<SDCVariable, SDCVariable>(start, end), null);
			cycleEdges.put(new Pair<SDCVariable, SDCVariable>(end, start), null);
		}

		// Find edges included in cycle
		for (SDCFeasibilityGraphEdge e : edges) {
			if (cycleEdges.containsKey(e.getVertices())) {
				cycleEdges.put(e.getVertices(), e.getWeight());
			}
		}

		File file = new File(filename);
		System.out.println("Printing negative cycle to file: " + file.getAbsolutePath());
		try {
			FileOutputStream os = new FileOutputStream(file, false);
			PrintStream printer = new PrintStream(os);
			printer.println("digraph{");
			for (Pair<SDCVariable, SDCVariable> edge : cycleEdges.keySet()) {
				if (cycleEdges.get(edge) != null) {
					String start = edge.getFirst().getName().replaceAll(" ", "");
					String end = edge.getSecond().getName().replaceAll(" ", "");
					printer.println(start + " -> " + end + " [label=\"" + cycleEdges.get(edge) + "\"];");
				}
			}
			printer.println("}");
			printer.close();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
