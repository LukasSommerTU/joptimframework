/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.debug;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import framework.Solveable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;

/**
 * Printer for linear problems
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPPrinter {

	/**
	 * Print textual representation of a linear problem to given file
	 * 
	 * @param fileName File
	 * @param lp Linear problem
	 */
	public static void printLP(String fileName, Solveable lp) {
		try {
			PrintWriter writer = new PrintWriter(fileName);
			writer.println("|**** Objective function ****|");
			writer.println(lp.getObjectiveFunction().toString());
			writer.println("\n");
			writer.println("|**** Constraints ****|");
			for (OptimLinearConstraint cons : lp.getConstraints()) {
				writer.println(cons.toString());
			}
			writer.println("\n");
			writer.println("|**** Variable specifications ****|");
			for (OptimVariable var : lp.getVariables()) {
				OptimLinearVariableConstraint varcons = lp.getVariableConstraint(var);
				if (varcons != null) {
					writer.println(varcons.toString());
				} else {
					writer.println(var.getName() + " REAL");
				}

			}
			writer.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Could not open file to write LP to it!", e);
		}

	}
}
