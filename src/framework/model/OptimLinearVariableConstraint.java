/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.model;

import framework.lp.model.LPVariable;

/**
 * Constrain the domain of a variable.
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class OptimLinearVariableConstraint {

	/**
	 * Upper bound
	 */
	private final double upperBound;
	
	/**
	 * Lower bound
	 */
	private final double lowerBound;
	
	/**
	 * Variable with which this constraint is associated
	 */
	private final OptimVariable var;
	
	/**
	 * Constrain the variable to the given domain
	 * @param variable Variable
	 * @param lB Lower bound
	 * @param uB Upper bound
	 */
	public OptimLinearVariableConstraint(OptimVariable variable, double lB, double uB){
		upperBound = uB;
		lowerBound = lB;
		var = variable;
	}

	/**
	 * Get the upper bound. 
	 * @return The upper bound
	 */
	public double getUpperBound() {
		return upperBound;
	}

	/**
	 * Get the lower bound
	 * @return the lower bound
	 */
	public double getLowerBound() {
		return lowerBound;
	}

	/**
	 * Associated variable
	 * @return Associated variable
	 */
	public OptimVariable getVar() {
		return var;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getLowerBound()+" =< ");
		sb.append(var.getName());
		sb.append(" =< "+getUpperBound());
		if(var instanceof LPVariable){
			if(((LPVariable) var).isOnlyBinary()){
				sb.append(" BIN\n");
			}
			else if(((LPVariable) var).isOnlyInteger()){
				sb.append(" INT\n");
			}
			else{
				sb.append(" REAL\n");
			}
		}
		else{
			sb.append(" REAL\n");
		}
		return sb.toString();
	}
}

