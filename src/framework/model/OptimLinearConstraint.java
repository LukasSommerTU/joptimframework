/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/**
 * Abstract super-class for linear constraints
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public abstract class OptimLinearConstraint {
	
	/** The Constant KEEP_OLD. */
	public static final int KEEP_OLD = -1;

	/** The Constant KEEP_BOTH. */
	public static final int KEEP_BOTH = 0;

	/** The Constant KEEP_NEW. */
	public static final int KEEP_NEW = 1;

	/** The Constant INFEASIBLE. */
	public static final int INFEASIBLE = 2;

	/** Constants to specify the relation between lhs and rhs of a constraint. */

	public static final int LESS_EQUAL = -1;

	/**
	 * The Constant EQUALS.
	 */
	public static final int EQUALS = 0;

	/** The Constant GREATER_EQUAL. */
	public static final int GREATER_EQUAL = 1;

	protected int type;

	protected double rhs;

	/**
	 * Set the right-hand side of the constant
	 * 
	 * @param constant RHS
	 */
	public void setRHS(double constant) {
		rhs = constant;
	}

	/**
	 * Get the right-hand side value
	 * 
	 * @return RHS-value
	 */
	public double getRHS() {
		return rhs;
	}

	/**
	 * Get the coefficient associated wit the variable in the constraint
	 * 
	 * @param var Variable to be looked up
	 * @return Coefficient or 0 if the variable is not contained in the constraint
	 */
	public abstract double getCoefficient(OptimVariable var);

	/**
	 * Get the relation between LHS and RHS
	 * 
	 * @return Integer constants specified in this class indicating the relation
	 */
	public int getType() {
		return type;
	}

	/**
	 * All variables contained in this constraint
	 * 
	 * @return Set of all contained variables
	 */
	public abstract Set<OptimVariable> getVariables();

	public int compareConstraints(OptimLinearConstraint cons){
		List<OptimVariable> vars1 = new LinkedList<OptimVariable>(this.getVariables());
		List<OptimVariable> vars2 = new LinkedList<OptimVariable>(cons.getVariables());
		Iterator<OptimVariable> it = vars1.iterator();
		while(it.hasNext()){
			OptimVariable var = it.next();
			if(this.getCoefficient(var)==0){
				it.remove();
			}
		}
		it = vars2.iterator();
		while(it.hasNext()){
			OptimVariable var = it.next();
			if(cons.getCoefficient(var)==0){
				it.remove();
			}
		}
		Comparator<OptimVariable> lexicalComparator = new Comparator<OptimVariable>() {

			@Override
			public int compare(OptimVariable o1, OptimVariable o2) {
				int comp = o1.getName().compareTo(o2.getName());
				if(comp==0){
					return 1;
				}
				return comp;
			}
		};
		Collections.sort(vars1, lexicalComparator);
		Collections.sort(vars2, lexicalComparator);
		if(vars1.equals(vars2)){
			boolean inversion = true;
			double [] coeff1 = new double[vars1.size()];
			double [] coeff2 = new double[vars1.size()];
			for(int i=0; i<vars1.size(); ++i){
				double c1 = this.getCoefficient(vars1.get(i));
				double c2 = cons.getCoefficient(vars2.get(i));
				if(c1 != c2){
					if(c1 != (c2 * -1) || !inversion){
						return KEEP_BOTH;
					}
					else if(c1 == (c2 * -1) && inversion){
						coeff1[i] = c1;
						coeff2[i] = c2 * -1;
					}
				}
				else{
					inversion = false;
					coeff1[i] = c1;
					coeff2[i] = c2;
				}
			}
			int relation = cons.getType();
			double rhs = cons.getRHS();
			if(inversion){
				relation = invertRelation(cons.getType());
				rhs = rhs * -1;
			}
			
			if(this.getType() == LESS_EQUAL){
				if(relation == LESS_EQUAL){
					if(rhs < this.getRHS()){
						// New constraint more restrictive
						return KEEP_NEW;
					}
					else{
						return KEEP_OLD;
					}
				}
				else if(relation == GREATER_EQUAL){
					if(rhs > this.getRHS()){
						// No intersection, infeasible
						return INFEASIBLE;
					}
					else{
						return KEEP_BOTH;
					}
				}
				else if(relation == EQUALS){
					if(rhs <= this.getRHS()){
						// Equality value in range, keep more restrictive equality
						return KEEP_NEW;
					}
					else{
						return INFEASIBLE;
					}
				}
			}
			else if(this.getType() == GREATER_EQUAL){
				if(relation == LESS_EQUAL){
					if(rhs < this.getRHS()){
						// No intersection, infeasible
						return INFEASIBLE;
					}
					else{
						return KEEP_BOTH;
					}
				}
				else if(relation == GREATER_EQUAL){
					if(rhs > this.getRHS()){
						// More restrictive, keep new
						return KEEP_NEW;
					}
					else{
						return KEEP_OLD;
					}
				}
				else if(relation == EQUALS){
					if(rhs >= this.getRHS()){
						// Equality value in range, keep more restrictive equality
						return KEEP_NEW;
					}
					else{
						return INFEASIBLE;
					}
				}
			}
			else if(this.getType() == EQUALS){
				if(relation == LESS_EQUAL){
					if(rhs >= this.getRHS()){
						// In range, keep equality
						return KEEP_OLD;
					}
					else{
						return INFEASIBLE;
					}
				}
				else if(relation == GREATER_EQUAL){
					if(rhs <= this.getRHS()){
						// In range, keep equality
						return KEEP_OLD;
					}
					else{
						return INFEASIBLE;
					}
				}
				else if(relation == EQUALS){
					if(rhs == this.getRHS()){
						// Equality with same value
						return KEEP_OLD;
					}
					else{
						return INFEASIBLE;
					}
				}
			}
		}
		return KEEP_BOTH;
	}
	
	private int invertRelation(int type2) {
		switch(type2){
			case LESS_EQUAL: return GREATER_EQUAL;
			case GREATER_EQUAL: return LESS_EQUAL;
			case EQUALS: return EQUALS;
		}
		return 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (OptimVariable var : getVariables()) {
			sb.append(' ');
			double coeff = getCoefficient(var);
			if (coeff > 0) {
				sb.append('+');
			} else if (coeff == 0) {
				continue;
			}
			sb.append(coeff).append(" * ");
			sb.append(var.toString());
		}

		switch (getType()) {
			case LESS_EQUAL:
				sb.append(" =< ");
				break;
			case EQUALS:
				sb.append(" = ");
				break;
			case GREATER_EQUAL:
				sb.append(" >= ");
		}

		sb.append(getRHS());
		return sb.toString();
	}

	/**
	 * Accept visitor.
	 *
	 * @param visitor the visitor
	 */
	public abstract void accept(ConstraintVisitor visitor);
}
