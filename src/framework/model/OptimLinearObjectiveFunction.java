/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Representation of the objective function
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class OptimLinearObjectiveFunction {

	/**
	 * Constants indicating the type of the objective function
	 */
	public static final int MAX = 1;

	/** The Constant MIN. */
	public static final int MIN = -1;

	/**
	 * Map each variable to its coefficient
	 */
	private final Map<OptimVariable, Double> coefficients = new HashMap<OptimVariable, Double>();

	/**
	 * Linear part
	 */
	private final double linearPart;

	private final int type;

	/**
	 * Construct new objective function
	 * @param linPart Linear part
	 * @param optimType Optimization type
	 */
	public OptimLinearObjectiveFunction(double linPart, int optimType) {
		type = optimType;
		linearPart = linPart;
	}

	/**
	 * Get the coefficient associated with the given variable.
	 *
	 * @param var the var
	 * @return the coefficient or 0 if the variable is not contained in the objective function
	 */
	public double getVarCoefficient(OptimVariable var) {
		if (coefficients.containsKey(var)) {
			double result = coefficients.get(var);
			return result;
		}
		return 0;
	}

	/**
	 * Adds the var coefficient.
	 *
	 * @param var the var
	 * @param coeff the coeff
	 */
	public void addVarCoefficient(OptimVariable var, double coeff) {
		coefficients.put(var, coeff);
	}

	/**
	 * Type of the objective function - maximize or minimize
	 * @return Integer constant specified in this class
	 */
	public int getOptimType() {
		return type;
	}

	/**
	 * Linear part of the objective function
	 * @return Linear part
	 */
	public double getLinearPart() {
		return linearPart;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (getOptimType() == OptimLinearObjectiveFunction.MAX) {
			sb.append("MAX ");
		} else {
			sb.append("MIN ");
		}
		for (OptimVariable var : coefficients.keySet()) {
			double coeff = getVarCoefficient(var);
			if (coeff != 0) {
				if (coeff < 0) {
					sb.append(coeff);
				} else {
					sb.append("+").append(+coeff);
				}
				sb.append(" " + var.toString() + " ");
			}
		}
		return sb.toString();
	}

}
