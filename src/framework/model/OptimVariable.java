/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.model;

/**
 * General object-representation for variables involved in optimization problems.
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class OptimVariable {
	
	/**
	 * Optional name for better usability.
	 */
	protected final String name;
	
	/**
	 * Optimal solution associated with this variable. 
	 */
	protected double optimSolution;
	
	
	/**
	 * Build new variable
	 */
	public OptimVariable(){
		name = "";
	}
	
	/**
	 * Build new variable with the given name
	 * @param label
	 */
	public OptimVariable(String label){
		name = label;
	}
	
	/**
	 * Check the associated optimal solution for integrality.
	 * @return <code>true</code> if the optimal solution is integral.
	 */
	public boolean hasIntegralOptimalSolution(){
		return (optimSolution == Math.rint(optimSolution));
	}

	/**
	 * Get the associated optimal solution.
	 * Returns Double.NaN if the problem was not 
	 * solved or is infeasible.
	 * @return Optimal solution
	 */
	public double getOptimalSolution() {
		return optimSolution;
	}

	/**
	 * Set the optimal solution
	 * @param optimSolution The optimal solution to set
	 */
	public void setOptimalSolution(double optimSolution) {
		this.optimSolution = optimSolution;
	}
	
	/**
	 * Reset the optimal solution to Double.NaN
	 */
	public void resetOptimalSolution(){
		this.optimSolution = Double.NaN;
	}

	/**
	 * Get the associated name
	 * @return the name Name or <code>null</code> if there is no.
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name.replaceAll(" ", "_");
	}
	
}
