package framework.model;

import framework.lp.model.LPLinearConstraint;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;

/**
 * Visitor for constraints
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public interface ConstraintVisitor {

	/**
	 * Visit @link{LPLinearConstraint}
	 * @param cons @link{LPLinearConstraint}
	 */
	public void visitLPLinearConstraint(LPLinearConstraint cons);

	/**
	 * Visit @link{SDCLinearConstraint}
	 * @param cons @link{SDCLinearConstraint}
	 */
	public void visitSDCLinearConstraint(SDCLinearConstraint cons);

	/**
	 * Visit @link{SDCSingleVariableConstraint}
	 * @param cons @link{SDCSingleVariableConstraint}
	 */
	public void visitSDCSingleVariableConstraint(SDCSingleVariableConstraint cons);
}
