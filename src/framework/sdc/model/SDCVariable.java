/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.model;

import framework.model.OptimVariable;

/**
 * SDC-specific variable implementation
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
/**
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDCVariable extends OptimVariable{
	
	private double feasibleSolution;
	
	/**
	 * Construct new variable with name
	 * @param label Name
	 */
	public SDCVariable(String label){
		super(label);
	}
	
	/**
	 * Construct new variable
	 */
	public SDCVariable() {
		super();
	}

	/**
	 * A feasible solution for this variable
	 * @return A feasible solution
	 */
	public double getFeasibleSolution() {
		return feasibleSolution;
	}

	/**
	 * Set a feasible solution
	 * @param feasibleSolution Feasible solution to set
	 */
	public void setFeasibleSolution(double feasibleSolution) {
		this.feasibleSolution = feasibleSolution;
	}

	/**
	 * Check feasible solution for integrity
	 * @return <code>true</code> if the feasible solution is integral
	 */
	public boolean hasIntegralFeasibleSolution(){
		return (feasibleSolution == Math.rint(feasibleSolution));
	}
	
}
