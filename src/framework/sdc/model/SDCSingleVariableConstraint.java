/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.model;

import java.util.HashSet;
import java.util.Set;

import framework.model.ConstraintVisitor;
import framework.model.OptimVariable;
import framework.sdc.SDC;

/**
 * Special constraint to model difference constraints of just one variable.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDCSingleVariableConstraint extends SDCLinearConstraint {

	/**
	 * Instantiates a new SDC single variable constraint.
	 *
	 * @param x the x
	 * @param constant the constant
	 * @param relation the relation
	 * @param sdc the sdc
	 */
	public SDCSingleVariableConstraint(SDCVariable x, double constant, int relation, SDC sdc) {
		super(x, sdc.getDummyVar(), constant, relation);
	}

	/**
	 * Gets the variable.
	 *
	 * @return the variable
	 */
	public SDCVariable getVariable() {
		return this.getVariable1();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.sdc.model.SDCLinearConstraint#accept(framework.model. ConstraintVisitor)
	 */
	@Override
	public void accept(ConstraintVisitor visitor) {
		visitor.visitSDCSingleVariableConstraint(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.sdc.model.SDCLinearConstraint#getVariables()
	 */
	@Override
	public Set<OptimVariable> getVariables() {
		Set<OptimVariable> result = new HashSet<OptimVariable>();
		result.add(getVariable());
		return result;
	}

}
