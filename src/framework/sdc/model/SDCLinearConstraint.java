/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.model;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import framework.model.ConstraintVisitor;
import framework.model.OptimLinearConstraint;
import framework.model.OptimVariable;
import framework.sdc.feasibility_checker.model.SDCFeasibilityGraphEdge;
import framework.sdc.feasibility_checker.model.util.Pair;

/**
 * Special constraint to model difference constraints of two variables.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDCLinearConstraint extends OptimLinearConstraint {

	private final SDCVariable var1;

	private final SDCVariable var2;

	/**
	 * Build new constraint x - y ? constant
	 * 
	 * @param x Variable with coefficient 1
	 * @param y Variable with coefficient -1
	 * @param constant Right hand side of the difference constraint
	 * @param equationType Relation between left and right hand side {@link OptimLinearConstraint}
	 */
	public SDCLinearConstraint(SDCVariable x, SDCVariable y, double constant, int equationType) {
		var1 = x;
		var2 = y;
		rhs = constant;
		type = equationType;
	}

	/**
	 * First variable
	 * 
	 * @return First variable
	 */
	public SDCVariable getVariable1() {
		return var1;
	}

	/**
	 * Second variable
	 * 
	 * @return Second variable
	 */
	public SDCVariable getVariable2() {
		return var2;
	}

	/**
	 * Get the edge representation of the difference constraint
	 * 
	 * @return Edge representation
	 */
	public Set<SDCFeasibilityGraphEdge> getEdgeRepresentation() {
		Set<SDCFeasibilityGraphEdge> result = new LinkedHashSet<SDCFeasibilityGraphEdge>();
		if (getType() == LESS_EQUAL || getType() == EQUALS) {
			result.add(new SDCFeasibilityGraphEdge(var2, var1, rhs));
		}
		if (getType() == GREATER_EQUAL || getType() == EQUALS) {
			result.add(new SDCFeasibilityGraphEdge(var1, var2, (rhs * -1)));
		}
		return result;
	}

	/**
	 * Checks if is similar constraint.
	 *
	 * @param toCompare the to compare
	 * @return Constant indicating similarity
	 */
	public int isSimilarConstraint(SDCLinearConstraint toCompare) {
		if (getVariablePair().equals(toCompare.getVariablePair())) {
			int compType = toCompare.getType();
			double compCons = toCompare.getRHS();
			if (type == EQUALS) {
				if (compType == type) {
					if (compCons != rhs) {
						// Two equalities with different constant
						return INFEASIBLE;
					} else {
						// Two equalities with same constant
						return KEEP_OLD;
					}
				} else if (compType == LESS_EQUAL) {
					if (compCons >= rhs) {
						// Range defined by other constraint includes this
						// equality
						return KEEP_OLD;
					} else {
						return INFEASIBLE;
					}
				} else if (compType == GREATER_EQUAL) {
					if (compCons <= rhs) {
						// Range defined by other constraint includes this
						// equality
						return KEEP_OLD;
					} else {
						return INFEASIBLE;
					}
				}
			} else if (type == LESS_EQUAL) {
				if (compType == type) {
					if (compCons >= rhs) {
						// Old constant is more strict
						return KEEP_OLD;
					} else {
						// New constraint is more strict
						return KEEP_NEW;
					}
				} else if (compType == EQUALS) {
					if (rhs < compCons) {
						return INFEASIBLE;
					} else {
						return KEEP_NEW;
					}
				} else if (compType == GREATER_EQUAL) {
					if (compCons > rhs) {
						return INFEASIBLE;
					} else {
						return KEEP_BOTH;
					}
				}
			} else if (type == GREATER_EQUAL) {
				if (compType == type) {
					if (compCons <= rhs) {
						// Old constraint is more strict
						return KEEP_OLD;
					} else {
						// New constraint is more strict
						return KEEP_NEW;
					}
				} else if (compType == EQUALS) {
					if (rhs > compCons) {
						return INFEASIBLE;
					} else {
						return KEEP_NEW;
					}
				} else if (compType == LESS_EQUAL) {
					if (compCons < rhs) {
						return INFEASIBLE;
					} else {
						return KEEP_BOTH;
					}
				}
			}
		}

		if (var1.equals(toCompare.getVariable2()) && var2.equals(toCompare.getVariable1())) {
			SDCVariable new1 = toCompare.getVariable2();
			SDCVariable new2 = toCompare.getVariable1();
			int newRelation = 0;
			switch (toCompare.getType()) {
				case EQUALS:
					newRelation = EQUALS;
					break;
				case LESS_EQUAL:
					newRelation = GREATER_EQUAL;
					break;
				case GREATER_EQUAL:
					newRelation = LESS_EQUAL;
					break;
			}
			double newConstant = toCompare.getRHS() * -1;
			return isSimilarConstraint(new SDCLinearConstraint(new1, new2, newConstant, newRelation));
		}

		//
		// if(getVariablePair().equals(toCompare.getVariablePair())){
		// int compType = toCompare.getType();
		// double compCons = toCompare.getRHS();
		// // Identical constraints
		// if(compType == type && rhs == compCons){
		// return KEEP_OLD;
		// }
		// else if(compType == type){
		// // Both constraints are equalities but have different constants
		// if(type == EQUALS){
		// return INFEASIBLE;
		// }
		// // If both are less-than relations, the one with the smaller constant
		// is more restrictive
		// else if(type == LESS_EQUAL){
		// if(rhs <= compCons){
		// return KEEP_OLD;
		// }
		// else{
		// return KEEP_NEW;
		// }
		// }
		// }
		// else if(type == EQUALS && compType == LESS_EQUAL){
		// if(rhs > compCons){
		// return INFEASIBLE;
		// }
		// return KEEP_OLD;
		// }
		// else if(type == LESS_EQUAL && compType == EQUALS){
		// if(compCons > rhs){
		// return INFEASIBLE;
		// }
		// return KEEP_NEW;
		// }
		//
		// }
		return KEEP_BOTH;
	}

	/**
	 * Gets the variable pair.
	 *
	 * @return the variable pair
	 */
	public Pair<SDCVariable, SDCVariable> getVariablePair() {
		return new Pair<SDCVariable, SDCVariable>(var2, var1);
	}

	/**
	 * Get coefficient associated with variable
	 * 
	 * @param var Variable
	 * @return Coefficient associated with variable
	 */
	@Override
	public double getCoefficient(OptimVariable var) {
		if (var.equals(var1)) {
			return 1;
		}
		if (var.equals(var2)) {
			return -1;
		}
		return 0;
	}

	/**
	 * Get involved variables
	 * 
	 * @return Set of the two involved variables
	 */
	@Override
	public Set<OptimVariable> getVariables() {
		Set<OptimVariable> result = new HashSet<OptimVariable>();
		result.add(var1);
		result.add(var2);
		return result;
	}

	@Override
	public void accept(ConstraintVisitor visitor) {
		visitor.visitSDCLinearConstraint(this);
	}

}
