/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import framework.Solveable;
import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;
import framework.sdc.feasibility_checker.SDCFeasibilityChecker;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;
import framework.sdc.observable.SDCObservable;
import framework.sdc.observer.SDCObserver;

/**
 * Object representation of a system of difference constraints
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDC extends SDCObservable implements Solveable {

	private LPSolver solver;
	
	private SDCFeasibilityChecker feasibilityChecker;

	private final Set<SDCLinearConstraint> constraints = new HashSet<SDCLinearConstraint>();

	private final Set<SDCVariable> variables = new HashSet<SDCVariable>();

	private OptimLinearObjectiveFunction objectiveFunction;

	private boolean isFeasible = true;

	private boolean isSolved = false;

	private final SDCVariable dummyVar = new SDCVariable("DummyVar");

	/**
	 * Add a new constraint to the system
	 * 
	 * @param constraint New constraint
	 */
	public void addConstraint(SDCLinearConstraint constraint) {
		invalidateSolution();
		SDCVariable var1 = constraint.getVariable1();
		SDCVariable var2 = constraint.getVariable2();
		if (!variables.contains(var1)) {
			variables.add(var1);
			sendVariableAddedNotification(var1);
		}
		if (!variables.contains(var2)) {
			variables.add(var2);
			sendVariableAddedNotification(var2);
		}
		constraints.add(constraint);
		isFeasible = sendConstraintAddedNotification(constraint);
	}

	/**
	 * Remove constraint from the system
	 * 
	 * @param constraint Constraint to remove
	 * @return <code>true</code> if the constraint was present and removed
	 */
	public boolean removeConstraint(SDCLinearConstraint constraint) {
		if (!constraints.contains(constraint)) {
			return false;
		}
		constraints.remove(constraint);
		isFeasible = sendConstraintRemovedNotification(constraint);
		invalidateSolution();
		return true;
	}

	/**
	 * Replace a constraint
	 * 
	 * @param constraint Old constraint
	 * @param newConstant New constraint
	 * @return <code>true</code> if the old constraint was present and removed
	 */
	/*
	 * Alias for remove, update and add again.
	 */
	public boolean updateConstraint(SDCLinearConstraint constraint, double newConstant) {
		if (!constraints.contains(constraint)) {
			return false;
		}
		invalidateSolution();
		removeConstraint(constraint);
		constraint.setRHS(newConstant);
		addConstraint(constraint);
		return true;
	}

	/**
	 * Check the system for feasibility.
	 * 
	 * @return <code>true</code> if the system is feasible
	 */
	public boolean checkFeasibility() {
		if (feasibilityChecker != null) {
			isFeasible = feasibilityChecker.checkFeasibility(this);
		}
		if (isFeasible) {
			associateVarWithSolution();
		} else {
			invalidateVarSolution();
		}

		return isFeasible;
	}

	/**
	 * Get the set solver
	 * 
	 * @return The solver
	 */
	@Override
	public LPSolver getSolver() {
		return solver;
	}

	/**
	 * Set new solver
	 * 
	 * @param solver The solver to set
	 */
	@Override
	public void setSolver(LPSolver solver) {
		invalidateSolution();
		this.solver = solver;
	}

	/**
	 * Get the feasibility checker
	 * 
	 * @return The feasibility checker
	 */
	public SDCFeasibilityChecker getFeasibilityChecker() {
		return feasibilityChecker;
	}

	/**
	 * Set feasibility checker to set
	 * 
	 * @param feasibilityChecker the feasibility checker to set
	 */
	public void setFeasibilityChecker(SDCFeasibilityChecker feasibilityChecker) {
		this.feasibilityChecker = feasibilityChecker;
	}

	/**
	 * Get all constraints contained in the system
	 * 
	 * @return the constraints Set of all constraints
	 */
	@Override
	public Set<SDCLinearConstraint> getConstraints() {
		return constraints;
	}

	/**
	 * Get the associated objective function
	 * 
	 * @return The objective function
	 */
	@Override
	public OptimLinearObjectiveFunction getObjectiveFunction() {
		return objectiveFunction;
	}

	/**
	 * All variables involved in the system
	 * 
	 * @return the variables Set of all variables
	 */
	@Override
	public Set<SDCVariable> getVariables() {
		return variables;
	}

	@Override
	public boolean isRestrictedNonNegative() {
		return true;
	}

	/**
	 * Solve the system with the solver set
	 */
	@Override
	public boolean solve() {
		if (solver == null) {
			throw new RuntimeException("No solver specified to solve LP!");
		}
		if (objectiveFunction == null) {
			throw new RuntimeException("No objective function specified! Specify an objective function before you solve the LP!");
		}
		if (!isSolved) {
			// Reset optimal solutions
			for(SDCVariable v : variables){
				v.resetOptimalSolution();
			}
			isSolved = solver.solve(this);
		}
		return isSolved;
	}

	/**
	 * Check the optimal solution for integrity
	 */
	@Override
	public boolean hasIntegralOptimalSolution() {
		if (isSolved) {
			for (SDCVariable var : variables) {
				if (!var.hasIntegralOptimalSolution()) {
					return false;
				}
			}
			return true;
		}
		System.err.println("Solve system first");
		return false;
	}

	private void associateVarWithSolution() {
		if (feasibilityChecker == null) {
			return;
		}
		Map<SDCVariable, Double> solution = feasibilityChecker.getFeasibleSolution();
		// System.out.println("Solution: "+solution);
		double dummyVal = solution.get(dummyVar);
		dummyVar.setFeasibleSolution(dummyVal);
		for (SDCVariable var : variables) {
			if (!var.equals(dummyVar)) {
				/*
				 * For all variables subtract feasible solution of the dummy
				 * variable to match single-variable constraints
				 */
				double varVal = solution.get(var) - dummyVal;
				var.setFeasibleSolution(varVal);
			}
		}
	}

	private void invalidateVarSolution() {
		for (SDCVariable var : variables) {
			var.setFeasibleSolution(Double.NaN);
		}
	}

	/**
	 * SDC dummy variable to enable modeling of single-variable constraints.
	 * 
	 * @return Dummy variable
	 */
	public SDCVariable getDummyVar() {
		return dummyVar;
	}

	/**
	 * Set a objective function
	 * 
	 * @param objectiveFunction The objective function to set
	 */
	@Override
	public void setObjectiveFunction(OptimLinearObjectiveFunction objfunc) {
		invalidateSolution();
		this.objectiveFunction = objfunc;
	}

	@Override
	public OptimLinearVariableConstraint getVariableConstraint(OptimVariable var) {
		return null;
	}

	@Override
	public void setRestrictedNonNegative(boolean restrictedNonNegative) {
		// Nothing to do;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * framework.sdc.observable.SDCObservable#addObserver(framework.sdc.observer
	 * .SDCObserver)
	 */
	@Override
	public void addObserver(SDCObserver observer) {
		super.addObserver(observer);
		observer.VariableAdded(this, dummyVar);
	}

	@Override
	public Set<? extends OptimLinearVariableConstraint> getVariableConstraints() {
		return new HashSet<OptimLinearVariableConstraint>();
	}

	private void invalidateSolution(){
		isSolved = false;
	}

	@Override
	public Set<OptimLinearConstraint> removeDuplicates() {
		ArrayList<OptimLinearConstraint> cons = new ArrayList<OptimLinearConstraint>(this.getConstraints());
		label: for(int i=0; i<cons.size(); ++i){
			OptimLinearConstraint c = cons.get(i);
			for(int j=i+1; j<cons.size(); ++j){
				OptimLinearConstraint o = cons.get(j);
				int result = c.compareConstraints(o);
				if(result==OptimLinearConstraint.INFEASIBLE){
					return null;
				}
				else if(result==OptimLinearConstraint.KEEP_OLD){
					cons.remove(o);
					--j;
				}
				else if(result==OptimLinearConstraint.KEEP_NEW){
					cons.remove(c);
					--i;
					continue label;
				}
			}
		}
		return new HashSet<OptimLinearConstraint>(cons);
	}

}
