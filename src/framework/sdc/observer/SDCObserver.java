/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.observer;

import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;
import framework.sdc.observable.SDCObservable;

/**
 * An asynchronous update interface for receiving notifications about SDC information as the SDC is constructed.
 */
public interface SDCObserver {

	/**
	 * This method is called when information about an SDC which was previously requested using an asynchronous interface becomes available.
	 *
	 * @param observable the observable
	 * @param addedConstraint the added constraint
	 * @return true, if constraint added
	 */
	public boolean ConstraintAdded(SDCObservable observable, SDCLinearConstraint addedConstraint);

	/**
	 * This method is called when information about an SDC which was previously requested using an asynchronous interface becomes available.
	 *
	 * @param observable the observable
	 * @param removedConstraint the removed constraint
	 * @return true, if constraint removed
	 */
	public boolean ConstraintRemoved(SDCObservable observable, SDCLinearConstraint removedConstraint);

	/**
	 * This method is called when information about an SDC which was previously requested using an asynchronous interface becomes available.
	 *
	 * @param observable the observable
	 * @param newVariable the new variable
	 */
	public void VariableAdded(SDCObservable observable, SDCVariable newVariable);
}
