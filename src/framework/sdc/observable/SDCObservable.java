/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.observable;

import java.util.HashSet;
import java.util.Set;

import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;
import framework.sdc.observer.SDCObserver;

/**
 * The Class SDCObservable.
 */
public class SDCObservable {

	private final Set<SDCObserver> observers = new HashSet<SDCObserver>();

	/**
	 * Adds the observer.
	 *
	 * @param observer the observer
	 */
	public void addObserver(SDCObserver observer) {
		observers.add(observer);
	}

	protected boolean sendConstraintAddedNotification(SDCLinearConstraint addedConstraint) {
		boolean result = false;
		for (SDCObserver o : observers) {
			result |= o.ConstraintAdded(this, addedConstraint);
		}
		return result;
	}

	protected boolean sendConstraintRemovedNotification(SDCLinearConstraint removedConstraint) {
		boolean result = false;
		for (SDCObserver o : observers) {
			result |= o.ConstraintRemoved(this, removedConstraint);
		}
		return result;
	}

	protected void sendVariableAddedNotification(SDCVariable newVariable) {
		for (SDCObserver o : observers) {
			o.VariableAdded(this, newVariable);
		}
	}
}
