/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import framework.debug.JOptimFrameworkDebugUtil;
import framework.sdc.SDC;
import framework.sdc.feasibility_checker.model.SDCFeasibilityGraphEdge;
import framework.sdc.feasibility_checker.model.util.Pair;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;

/**
 * Feasibility checker based on negative cycle detection using
 * Bellman-Ford-algorithm.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class BellmanFordChecker implements SDCFeasibilityChecker {

	private final Map<SDCLinearConstraint, Set<SDCFeasibilityGraphEdge>> constraintEdges = new HashMap<SDCLinearConstraint, Set<SDCFeasibilityGraphEdge>>();

	private final Map<SDCVariable, SDCVariable> predecessors = new HashMap<SDCVariable, SDCVariable>();

	private final Map<SDCVariable, Double> distance = new HashMap<SDCVariable, Double>();

	private final Set<SDCFeasibilityGraphEdge> edges = new HashSet<SDCFeasibilityGraphEdge>();

	private final SDCVariable startNode = new SDCVariable("Feasibility Graph start node");

	private final Set<SDCVariable> variables = new HashSet<SDCVariable>();

	protected boolean debug = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * framework.sdc.feasibility_checker.SDCFeasibilityChecker#checkFeasibility
	 * ()
	 */
	@Override
	public boolean checkFeasibility(SDC sdc) {
		if (performBellmanFordCheck(sdc) != null) {
			return true;
		}
		return false;
	}

	/**
	 * Check the system for feasibility by checking the graph for negative
	 * cycles with Bellman-Ford-algorithm.
	 * 
	 * @param sdc
	 * @return Mapping from each variable to its feasible solution
	 */
	public Map<SDCVariable, Double> performBellmanFordCheck(SDC sdc) {
		predecessors.clear();
		constraintEdges.clear();
		distance.clear();
		edges.clear();
		variables.clear();
		addVariable(sdc.getDummyVar());
		if (!buildFeasibilityGraph(sdc)) {
			if (debug) {
				System.err.println("Infeasible system!");
			}
			return null;
		} else {
			distance.put(startNode, 0.0);
			for (int i = 0; i < sdc.getVariables().size() + 1; ++i) {
				for (SDCFeasibilityGraphEdge edge : edges) {
					SDCVariable u = edge.getEdgeStart();
					SDCVariable v = edge.getEdgeEnd();
					double compDist = distance.get(u) + edge.getWeight();
					if (compDist < distance.get(v)) {
						distance.put(v, compDist);
						predecessors.put(v, u);
					}
				}
			}
			/*
			 * Check for negative cycles
			 */
			for (SDCFeasibilityGraphEdge edge : edges) {
				SDCVariable u = edge.getEdgeStart();
				// System.out.println(u);
				SDCVariable v = edge.getEdgeEnd();
				// System.out.println(v);
				// System.out.println("u: "+distance.get(u)+" edge: "+edge.getWeight()+" v: "+distance.get(v));
				double compDist = distance.get(u) + edge.getWeight();
				if (compDist < distance.get(v)) {
					if (debug) {
						System.err.println("Negative cycle detected!");
					}
					// Identify the elements of the negative cycle
					SDCVariable pred;
					LinkedList<SDCVariable> cycle = new LinkedList<SDCVariable>();
					cycle.add(v);
					pred = u;
					while (!pred.equals(v) && !cycle.contains(pred)) {
						cycle.addFirst(pred);
						pred = predecessors.get(pred);
					}
					cycle.addFirst(pred);
					if (debug) {
						JOptimFrameworkDebugUtil.printNegativeCycle("NegativeCycle" + sdc.hashCode() + ".dot", cycle, edges);
						JOptimFrameworkDebugUtil.printFeasibilityGraph("FeasibilityGraph" + sdc.hashCode() + ".dot", new LinkedList<SDCFeasibilityGraphEdge>(edges), false);
					}
					return null;
				}
			}
		}
		return distance;
	}

	/**
	 * Build feasibility graph from system and care for multi-edges
	 * 
	 * @param sdc System of difference constraints
	 * @return <code>true</code> if a feasibility graph was built.
	 */
	private boolean buildFeasibilityGraph(SDC sdc) {

		Map<Pair<SDCVariable, SDCVariable>, SDCLinearConstraint> edgeVariables = new HashMap<Pair<SDCVariable, SDCVariable>, SDCLinearConstraint>();

		Pair<SDCVariable, SDCVariable> pair;
		for (SDCLinearConstraint cons : sdc.getConstraints()) {
			pair = cons.getVariablePair();
			/*
			 * Multi-edge for this variable pair, choose the stricter one. If
			 * the edges are contradicting report infeasibility.
			 */
			if (edgeVariables.containsKey(pair)) {
				SDCLinearConstraint otherCons = edgeVariables.get(pair);
				int comparison = otherCons.isSimilarConstraint(cons);
				switch (comparison) {
					case SDCLinearConstraint.INFEASIBLE:
						return false;
					case SDCLinearConstraint.KEEP_OLD:
						break;
					case SDCLinearConstraint.KEEP_NEW:
						replaceEdge(otherCons, cons);
						edgeVariables.put(pair, cons);
						break;
					default:
						throw new RuntimeException("Unable to construct feasibility graph without multi-vertices!");
				}
			} else {
				addVariable(cons.getVariable1());
				addVariable(cons.getVariable2());
				Set<SDCFeasibilityGraphEdge> newEdges = cons.getEdgeRepresentation();
				edges.addAll(newEdges);
				constraintEdges.put(cons, newEdges);
				edgeVariables.put(cons.getVariablePair(), cons);
			}
		}
		return true;
	}

	/**
	 * Replace edge in the system
	 * 
	 * @param oldCons Old edge
	 * @param newCons New edge
	 */
	private void replaceEdge(SDCLinearConstraint oldCons, SDCLinearConstraint newCons) {
		for (SDCFeasibilityGraphEdge oldEdge : constraintEdges.get(oldCons)) {
			edges.remove(oldEdge);
		}
		constraintEdges.remove(oldCons);
		Set<SDCFeasibilityGraphEdge> newEdges = newCons.getEdgeRepresentation();
		edges.addAll(newEdges);
		constraintEdges.put(newCons, newEdges);
	}

	/**
	 * Add variable to the system and initialize values
	 * 
	 * @param var New variable
	 */
	private void addVariable(SDCVariable var) {
		if (variables.contains(var)) {
			return;
		}
		variables.add(var);
		predecessors.put(var, null);
		distance.put(var, Double.POSITIVE_INFINITY);
		edges.add(new SDCFeasibilityGraphEdge(startNode, var, 0));
	}

	/**
	 * Enable or disable debugging-output (dot-files)
	 * 
	 * @param mode True to enable debugging-output
	 */
	public void setDebugMode(boolean mode) {
		debug = mode;
	}

	/**
	 * Check if the debugging-output (dot-files) is enabled or not
	 * 
	 * @return True if debugging-output is enabled
	 */
	public boolean getDebugMode() {
		return debug;
	}

	@Override
	public Map<SDCVariable, Double> getFeasibleSolution() {
		return distance;
	}
}
