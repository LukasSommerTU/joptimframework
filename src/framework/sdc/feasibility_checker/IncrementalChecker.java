/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import framework.sdc.SDC;
import framework.sdc.feasibility_checker.algo.Heap;
import framework.sdc.feasibility_checker.model.SDCFeasibilityGraphEdge;
import framework.sdc.feasibility_checker.model.util.Pair;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;
import framework.sdc.observable.SDCObservable;
import framework.sdc.observer.SDCObserver;

/**
 * Feasibility checker using incremental, graph-based algorithm
 */
public class IncrementalChecker implements SDCObserver, SDCFeasibilityChecker {

	protected SDCVariable startNode = new SDCVariable("StartNode_FeasibilityGraph");

	protected final Map<SDCVariable, Set<SDCVariable>> successors = new HashMap<SDCVariable, Set<SDCVariable>>();

	protected final Map<Pair<SDCVariable, SDCVariable>, Set<SDCFeasibilityGraphEdge>> edges = new HashMap<Pair<SDCVariable, SDCVariable>, Set<SDCFeasibilityGraphEdge>>();

	protected final Map<Pair<SDCVariable, SDCVariable>, Double> length = new HashMap<Pair<SDCVariable, SDCVariable>, Double>();

	protected Map<SDCVariable, Double> feasibleSolution = new HashMap<SDCVariable, Double>();

	protected final Map<Pair<SDCVariable, SDCVariable>, SDCFeasibilityGraphEdge> unprocessed = new HashMap<Pair<SDCVariable, SDCVariable>, SDCFeasibilityGraphEdge>();

	protected final boolean debug = false;

	/**
	 * Instantiates a new incremental checker.
	 */
	public IncrementalChecker() {
		successors.put(startNode, new HashSet<SDCVariable>());
		feasibleSolution.put(startNode, 0.0);
	}

	@Override
	public boolean checkFeasibility(SDC sdc) {
		return unprocessed.isEmpty();
	}

	@Override
	public boolean ConstraintAdded(SDCObservable observable, SDCLinearConstraint addedConstraint) {
		boolean result = true;
		// System.out.println("Processing constraint: "+addedConstraint.toString());
		// One constraint may result in multiple edges (e.g. equalities)
		for (SDCFeasibilityGraphEdge edge : addedConstraint.getEdgeRepresentation()) {
			Pair<SDCVariable, SDCVariable> vertices = edge.getVertices();
			/*
			 * Multi-edge, for this variable-pair an edge was already in the graph.
			 */
			if (length.containsKey(vertices)) {
				edges.get(vertices).add(edge);
				/*
				 * Check if the new edge has an impact on the graph - i.e. is more restrictive than the edge already present for the two variables (smaller RHS).
				 */
				if (edge.getWeight() < length.get(vertices)) {
					// Update length with new, shorter edge
					length.put(vertices, edge.getWeight());
					// This edge has an impact on the graph
					if (unprocessed.containsKey(vertices)) {
						// Edge already present was unprocessed
						unprocessed.put(vertices, edge);
						result = false;
					} else {
						// Update graph with new, more restrictive edge
						if (!addToFeasible(vertices)) {
							// Graph becomes infeasible with new edge, remove
							// edge from graph.
							unprocessed.put(vertices, edge);
							successors.get(edge.getEdgeStart()).remove(edge.getEdgeEnd());
							result = false;
						}
					}
				}
			}
			/*
			 * First edge in the graph for this pair of variables.
			 */
			else {
				// Initialize data-structures
				Set<SDCFeasibilityGraphEdge> e = new HashSet<SDCFeasibilityGraphEdge>();
				e.add(edge);
				edges.put(vertices, e);
				length.put(vertices, edge.getWeight());
				if (!unprocessed.isEmpty()) {
					// Some edges are still unprocessed, add the new edge there
					unprocessed.put(vertices, edge);
					result = false;
				} else {
					successors.get(edge.getEdgeStart()).add(edge.getEdgeEnd());
					// Try to add the edge to the graph
					if (!addToFeasible(vertices)) {
						// Edge introduces infeasibility, remove from the graph
						// again.
						unprocessed.put(vertices, edge);
						successors.get(edge.getEdgeStart()).remove(edge.getEdgeEnd());
						result = false;
					}
				}
			}
		}

		return result & unprocessed.isEmpty();
	}

	protected boolean addToFeasible(Pair<SDCVariable, SDCVariable> constraint) {
		SDCFeasibilityGraphEdge current = new SDCFeasibilityGraphEdge(constraint.getFirst(), constraint.getSecond(), length.get(constraint));
		// System.out.println("Adding edge: "+current.toString());

		SDCVariable u = constraint.getFirst();
		SDCVariable v = constraint.getSecond();

		List<SDCFeasibilityGraphEdge> curEdges = new LinkedList<SDCFeasibilityGraphEdge>();
		for (SDCVariable s : successors.keySet()) {
			for (SDCVariable e : successors.get(s)) {
				if (!(s.equals(u) && e.equals(v))) {
					curEdges.add(new SDCFeasibilityGraphEdge(s, e, getEdgeLength(s, e)));
				}
			}
		}
		curEdges.add(current);
		// JOptimFrameworkDebugUtil.printFeasibilityGraph("incremental.dot",
		// curEdges, true);

		// Initialize new solution with the old one
		Map<SDCVariable, Double> newSol = new HashMap<SDCVariable, Double>(feasibleSolution);
		Heap<Double, SDCVariable> pq = new Heap<Double, SDCVariable>(Double.POSITIVE_INFINITY);
		pq.insertIntoHeap(0.0, v);
		while (!pq.isEmpty()) {
			Pair<SDCVariable, Double> min = pq.findAndDeleteMin();
			SDCVariable x = min.getFirst();
			double dist_x = min.getSecond();
			double dx = getLastFeasible(x);
			double du = getLastFeasible(u);
			double dv = getLastFeasible(v);
			double len_uv = getEdgeLength(u, v);
			double comp = du + len_uv + (dx + dist_x - dv);
			if (comp < newSol.get(x)) {
				// if(comp < dx){
				if (x.equals(u)) {
					// Negative cycle, system infeasible.
					return false;
				} else {
					// Update solution for x
					newSol.put(x, comp);
					// System.out.println("Updating: "+x.toString()+" from: "+dx+" to: "+
					// comp);
					for (SDCVariable y : successors.get(x)) {
						double dy = getLastFeasible(y);
						double len_xy = getEdgeLength(x, y);
						double scaledPathLength = dist_x + (dx + len_xy - dy);
						if (scaledPathLength < pq.keyOf(y)) {
							// System.out.println("Adjusting: "+y.toString()+" to: "+scaledPathLength+" old: "+pq.keyOf(y));
							pq.adjustHeap(scaledPathLength, y);
						}
					}
				}

			}
		}
		feasibleSolution = newSol;
		return true;
	}

	@Override
	public boolean ConstraintRemoved(SDCObservable observable, SDCLinearConstraint removedConstraint) {
		for (SDCFeasibilityGraphEdge edge : removedConstraint.getEdgeRepresentation()) {
			Pair<SDCVariable, SDCVariable> vertices = edge.getVertices();
			if (edges.get(vertices).size() > 1) {
				// There are multiple edges for this pair of variables
				if (length.get(vertices) < edge.getWeight()) {
					// Removed edge had no impact on the graph
					edges.get(vertices).remove(edge);
					return unprocessed.isEmpty();
				} else {
					double oldLen = length.get(vertices);
					edges.get(vertices).remove(edge);
					SDCFeasibilityGraphEdge resEdge = findMostRestrictive(edges.get(vertices));
					double newLen = resEdge.getWeight();
					if (unprocessed.containsKey(vertices)) {
						// Removed edge was unprocessed
						unprocessed.put(vertices, resEdge);
						if (newLen == oldLen) {
							// Graph did not become less restrictive, still
							// infeasible.
							return false;
						}
						// Update length
						length.put(vertices, newLen);
					} else {
						// Update length and compute new solution. (System must
						// be feasible as it is less restrictive now).
						length.put(vertices, newLen);
						addToFeasible(vertices);
					}
				}
			} else {
				// Removed edge is the only edge for this pair of variables
				edges.remove(vertices);
				length.remove(vertices);
				if (unprocessed.containsKey(vertices)) {
					unprocessed.remove(vertices);
				} else {
					// Edge was part of the graph, remove it.
					successors.get(edge.getEdgeStart()).remove(edge.getEdgeEnd());
				}
			}
		}
		// Graph is less restrictive now, try to add unprocessed edges.
		Iterator<Pair<SDCVariable, SDCVariable>> it = unprocessed.keySet().iterator();
		while (it.hasNext()) {
			Pair<SDCVariable, SDCVariable> unprocessedVertices = it.next();
			if (addToFeasible(unprocessedVertices)) {
				// Successfully added unprocessed edge.
				unprocessed.remove(unprocessedVertices);
			} else {
				// Could not add unprocessed edge, still infeasible;
				return false;
			}
		}
		return unprocessed.isEmpty();
	}

	@Override
	public void VariableAdded(SDCObservable observable, SDCVariable newVariable) {
		if (!successors.containsKey(newVariable)) {
			successors.put(newVariable, new HashSet<SDCVariable>());
			successors.get(startNode).add(newVariable);
			SDCFeasibilityGraphEdge edge = new SDCFeasibilityGraphEdge(startNode, newVariable, 0);
			HashSet<SDCFeasibilityGraphEdge> newSet = new HashSet<SDCFeasibilityGraphEdge>();
			newSet.add(edge);
			edges.put(edge.getVertices(), newSet);
			length.put(edge.getVertices(), 0.0);
			feasibleSolution.put(newVariable, 0.0);
		}

	}

	protected double getLastFeasible(SDCVariable v) {
		return feasibleSolution.get(v);
	}

	protected double getEdgeLength(Pair<SDCVariable, SDCVariable> nodes) {
		return length.get(nodes);
	}

	protected double getEdgeLength(SDCVariable start, SDCVariable end) {
		return getEdgeLength(new Pair<SDCVariable, SDCVariable>(start, end));
	}

	protected Set<SDCVariable> getSuccessors(SDCVariable v) {
		return successors.get(v);
	}

	private SDCFeasibilityGraphEdge findMostRestrictive(Set<SDCFeasibilityGraphEdge> set) {
		SDCFeasibilityGraphEdge result = null;
		double tmp = Double.POSITIVE_INFINITY;
		for (SDCFeasibilityGraphEdge e : set) {
			if (e.getWeight() < tmp) {
				result = e;
			}
		}
		return result;
	}

	@Override
	public Map<SDCVariable, Double> getFeasibleSolution() {
		return feasibleSolution;
	}
}
