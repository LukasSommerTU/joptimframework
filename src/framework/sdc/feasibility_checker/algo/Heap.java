/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker.algo;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import framework.sdc.feasibility_checker.model.util.Pair;

/**
 * Heap implementation for algorithmic purposes (e.g. Dijkstra's algorithm), each value can 
 * be associated with a key
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 * @param <Key> Type of the key, must implement {@link Comparable}
 * @param <Value> Type of the value
 */
public class Heap<Key extends Comparable<Key>, Value> {
	
	
	private final Map<Value, Key> keys = new HashMap<Value, Key>();
	
	private final Key defaultReturn;
	
	private final PriorityQueue<Value> queue = new PriorityQueue<Value>(1, new Comparator<Value>() {
		// Sort elements according to their associated key
		@Override
		public int compare(Value o1, Value o2) {
			return keys.get(o1).compareTo(keys.get(o2));
		}
	});
	
	/**
	 * Construct new Heap
	 * @param defaultValue Value to be returned if the queried element is not in the heap
	 */
	public Heap(Key defaultValue){
		defaultReturn = defaultValue;
	}

	/**
	 * Insert element into heap, associated with key
	 * @param k Key
	 * @param v Element
	 */
	public void insertIntoHeap(Key k, Value v){
		keys.put(v, k);
		if(queue.contains(v)){
			queue.remove(v);
		}
		queue.add(v);
	}
	
	/**
	 * Retrieve the element with the smallest key and at the same time its associated key
	 * @return {@link Pair} consisting of the element and its associated key
	 */
	public Pair<Value, Key> findAndDeleteMin(){
		Value v = queue.poll();
		return new Pair<Value, Key>(v, keys.get(v));
	}
	
	/**
	 * Change the key associated with a value in the heap.
	 * Inserts the given element associated with the given key if the element was not
	 * present before. 
	 * @param k New key
	 * @param v Element
	 */
	public void adjustHeap(Key k, Value v){
		if(queue.contains(v)){
			queue.remove(v);
		}
		keys.put(v, k);
		queue.add(v);
	}
	
	/**
	 * Retrieve the key associated with a element in the heap.
	 * @param v Element
	 * @return Key associated with the element or the value specified in the constructor
	 * when the element is not present in the heap
	 */
	public Key keyOf(Value v){
		if(queue.contains(v)){
			return keys.get(v);
		}
		return defaultReturn;
	}
	
	/**
	 * Check if the heap is empty, i.e. contains no element
	 * @return {@literal true} if the heap does not contain a element, false otherwise.
	 */
	public boolean isEmpty(){
		return queue.isEmpty();
	}
	
}
