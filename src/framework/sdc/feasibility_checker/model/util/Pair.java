/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker.model.util;

/**
 * Representation for a pair of two values.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 * @param <FirstType> Type of the first element of the pair
 * @param <SecondType> Type of the second element of the pair
 */
public class Pair<FirstType, SecondType> {

	private FirstType first;
	private SecondType second;

	/**
	 * Instantiates a new pair.
	 *
	 * @param firstVal the first val
	 * @param secondVal the second val
	 */
	public Pair(FirstType firstVal, SecondType secondVal) {
		first = firstVal;
		second = secondVal;
	}

	/**
	 * @return the first
	 */
	public FirstType getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(FirstType first) {
		this.first = first;
	}

	/**
	 * @return the second
	 */
	public SecondType getSecond() {
		return second;
	}

	/**
	 * @param second the second to set
	 */
	public void setSecond(SecondType second) {
		this.second = second;
	}

	// /* (non-Javadoc)
	// * @see java.lang.Object#equals(java.lang.Object)
	// */
	// @SuppressWarnings("rawtypes")
	// @Override
	// public boolean equals(Object obj) {
	// if(obj instanceof Pair){
	// Pair toCompare = (Pair) obj;
	// return first.equals(toCompare.getFirst()) &&
	// second.equals(toCompare.getSecond());
	// }
	// return super.equals(obj);
	// }
	//
	// /* (non-Javadoc)
	// * @see java.lang.Object#hashCode()
	// */
	// @Override
	// public int hashCode() {
	// return first.hashCode() + second.hashCode();
	// }
	//

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(10);
		sb.append('(');
		sb.append(first.toString());
		sb.append(", ");
		sb.append(second.toString());
		sb.append(")");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null) {
				return false;
			}
		} else if (!first.equals(other.first)) {
			return false;
		}
		if (second == null) {
			if (other.second != null) {
				return false;
			}
		} else if (!second.equals(other.second)) {
			return false;
		}
		return true;
	}

}
