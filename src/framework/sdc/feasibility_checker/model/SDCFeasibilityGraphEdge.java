/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker.model;

import framework.sdc.feasibility_checker.model.util.Pair;
import framework.sdc.model.SDCVariable;

/**
 * Object representation for a edge in the feasibility graph.
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDCFeasibilityGraphEdge {

	/**
	 * Start point/variable
	 */
	private final SDCVariable startpoint;

	/**
	 * End point/variable
	 */
	private final SDCVariable endpoint;

	/**
	 * Weight of the edge
	 */
	private double weight;

	/**
	 * If this is set to true, the dot-representation is highlighted in color.
	 */
	private boolean isHighlighted = false;

	/**
	 * Instantiates a new SDC feasibility graph edge.
	 *
	 * @param edgeStart the edge start
	 * @param egdeEnd the egde end
	 * @param edgeWeight the edge weight
	 */
	public SDCFeasibilityGraphEdge(SDCVariable edgeStart, SDCVariable egdeEnd, double edgeWeight) {
		startpoint = edgeStart;
		endpoint = egdeEnd;
		weight = edgeWeight;
	}

	/**
	 * Get the start point
	 * @return Start point
	 */
	public SDCVariable getEdgeStart() {
		return startpoint;
	}

	/**
	 * Get the end point
	 * @return End point
	 */
	public SDCVariable getEdgeEnd() {
		return endpoint;
	}

	/**
	 * Pair consisting of the start and end point
	 * @return Pair of start and end point
	 */
	public Pair<SDCVariable, SDCVariable> getVertices() {
		return new Pair<SDCVariable, SDCVariable>(startpoint, endpoint);
	}

	/**
	 * Get the edges weight
	 * @return Edge weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * Set the edges weight
	 * @param newEdgeWeight Weight to set
	 */
	public void setWeight(double newEdgeWeight) {
		weight = newEdgeWeight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SDCFeasibilityGraphEdge) {
			SDCFeasibilityGraphEdge e = (SDCFeasibilityGraphEdge) obj;
			return e.getEdgeStart().equals(startpoint) && e.getEdgeEnd().equals(endpoint) && (e.getWeight() == weight);
		}
		return super.equals(obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return (int) (startpoint.hashCode() + endpoint.hashCode() + weight);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		sb.append(startpoint.toString());
		sb.append(" --(");
		sb.append(weight);
		sb.append(")--> ");
		sb.append(endpoint.toString());
		return sb.toString();
	}

	/**
	 * Dot representation.
	 *
	 * @return the string
	 */
	public String dotRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append(startpoint.toString());
		sb.append(" -> ");
		sb.append(endpoint.toString());
		sb.append(" [label=\"").append(weight).append("\"");
		if (isHighlighted) {
			sb.append(", color=\"red\"");
		}
		sb.append("];");
		return sb.toString();
	}

	/**
	 * Return if this edge shall be highlighted in the dot graph
	 * @return the isHighlighted
	 */
	public boolean isHighlighted() {
		return isHighlighted;
	}

	/**
	 * Determine whether this edge is highlighted in the dot graph
	 * @param isHighlighted True if this edge should be highlighted in the dot graph
	 */
	public void setHighlighted(boolean isHighlighted) {
		this.isHighlighted = isHighlighted;
	}

}
