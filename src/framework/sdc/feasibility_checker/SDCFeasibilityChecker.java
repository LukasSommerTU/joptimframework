/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.sdc.feasibility_checker;

import java.util.Map;

import framework.sdc.SDC;
import framework.sdc.model.SDCVariable;

/**
 * Interface for feasibility checkers.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public interface SDCFeasibilityChecker {

	/**
	 * Check system for feasibility
	 * 
	 * @param sdc System of difference constraints
	 * @return <code>true</true> if the system is feasible
	 */
	public boolean checkFeasibility(SDC sdc);

	/**
	 * Return the feasible solution for all variables
	 * 
	 * @return Mapping from each variable to a feasible value
	 */
	public Map<SDCVariable, Double> getFeasibleSolution();
}
