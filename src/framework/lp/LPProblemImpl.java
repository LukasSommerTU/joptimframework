/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimVariable;
import framework.model.OptimLinearVariableConstraint;

/**
 * General purpose implementation of a linear problem
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPProblemImpl implements LPProblem {

	/**
	 * LP-solver
	 */
	private LPSolver solver;

	/**
	 * All constraints
	 */
	private final Set<OptimLinearConstraint> constraints = new HashSet<OptimLinearConstraint>();

	/**
	 * All variables
	 */
	private final Set<OptimVariable> variables = new HashSet<OptimVariable>();

	/**
	 * Map each variable to its number of occurrences
	 */
	private final Map<OptimVariable, Integer> varoccurences = new HashMap<OptimVariable, Integer>();

	/**
	 * Variable constraints - upper and lower bound
	 */
	private final Map<OptimVariable, OptimLinearVariableConstraint> varconstraints = new HashMap<OptimVariable, OptimLinearVariableConstraint>();

	/**
	 * Objective function
	 */
	private OptimLinearObjectiveFunction objfun;

	private boolean nonNegative = false;

	/**
	 * Has the system been solved
	 */
	private boolean isSolved = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * framework.lp.LPProblem#addConstraint(framework.model.OptimLinearConstraint)
	 */
	@Override
	public void addConstraint(OptimLinearConstraint addedConstraint) {
		invalidateSolution();
		for (OptimVariable var : addedConstraint.getVariables()) {
			if (!variables.contains(var)) {
				variables.add(var);
				varoccurences.put(var, 1);
			} else {
				varoccurences.put(var, (varoccurences.get(var) + 1));
			}
		}
		constraints.add(addedConstraint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * framework.lp.LPProblem#removeConstraint(framework.model.OptimLinearConstraint)
	 */
	@Override
	public boolean removeConstraint(OptimLinearConstraint removedConstraint) {
		if (!constraints.contains(removedConstraint)) {
			return false;
		}
		invalidateSolution();
		constraints.remove(removedConstraint);
		for (OptimVariable var : removedConstraint.getVariables()) {
			if (varoccurences.get(var) == 1) {
				varoccurences.remove(var);
				variables.remove(var);
			} else {
				varoccurences.put(var, (varoccurences.get(var) - 1));
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#setSolver(framework.lp.solver.LPSolver)
	 */
	@Override
	public void setSolver(LPSolver solverInstance) {
		invalidateSolution();
		solver = solverInstance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#getSolver()
	 */
	@Override
	public LPSolver getSolver() {
		return solver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#getConstraints()
	 */
	@Override
	public Set<OptimLinearConstraint> getConstraints() {
		return constraints;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#getVariables()
	 */
	@Override
	public Set<OptimVariable> getVariables() {
		return variables;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#isRestrictedNonNegative()
	 */
	@Override
	public boolean isRestrictedNonNegative() {
		return varconstraints.isEmpty() && nonNegative;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#solve()
	 */
	@Override
	public boolean solve() {
		if (solver == null) {
			throw new RuntimeException("No solver specified to solve LP!");
		}
		if (objfun == null) {
			throw new RuntimeException("No objective function specified! Specify an objective function before you solve the LP!");
		}
		if (!isSolved) {
			for(OptimVariable v : variables){
				v.resetOptimalSolution();
			}
			isSolved = solver.solve(this);
		}
		return isSolved;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#hasIntegralOptimalSolution()
	 */
	@Override
	public boolean hasIntegralOptimalSolution() {
		if (isSolved) {
			for (OptimVariable var : variables) {
				if (!var.hasIntegralOptimalSolution()) {
					return false;
				}
			}
			return true;
		}
		System.err.println("Solve system first!");
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#setObjectiveFunction(framework.model.
	 * OptimLinearObjectiveFunction)
	 */
	@Override
	public void setObjectiveFunction(OptimLinearObjectiveFunction objfunc) {
		invalidateSolution();
		objfun = objfunc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#getObjectiveFunction()
	 */
	@Override
	public OptimLinearObjectiveFunction getObjectiveFunction() {
		return objfun;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#addVariableConstraint(framework.model.
	 * OptimLinearVariableConstraint)
	 */
	@Override
	public void addVariableConstraint(OptimLinearVariableConstraint constraint) {
		invalidateSolution();
		varconstraints.put(constraint.getVar(), constraint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * framework.lp.LPProblem#getVariableConstraint(framework.model.OptimVariable
	 * )
	 */
	@Override
	public OptimLinearVariableConstraint getVariableConstraint(OptimVariable var) {
		return varconstraints.get(var);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.lp.LPProblem#setRestrictedNonNegative(boolean)
	 */
	@Override
	public void setRestrictedNonNegative(boolean restrictedNonNegative) {
		invalidateSolution();
		nonNegative = restrictedNonNegative;
	}

	@Override
	public Set<? extends OptimLinearVariableConstraint> getVariableConstraints() {
		return new HashSet<OptimLinearVariableConstraint>(varconstraints.values());
	}

	private void invalidateSolution(){
		isSolved = false;
	}

	@Override
	public Set<OptimLinearConstraint> removeDuplicates() {
		ArrayList<OptimLinearConstraint> cons = new ArrayList<OptimLinearConstraint>(this.getConstraints());
		label: for(int i=0; i<cons.size(); ++i){
			OptimLinearConstraint c = cons.get(i);
			for(int j=i+1; j<cons.size(); ++j){
				OptimLinearConstraint o = cons.get(j);
				int result = c.compareConstraints(o);
				if(result==OptimLinearConstraint.INFEASIBLE){
					return null;
				}
				else if(result==OptimLinearConstraint.KEEP_OLD){
					cons.remove(o);
					--j;
				}
				else if(result==OptimLinearConstraint.KEEP_NEW){
					cons.remove(c);
					--i;
					continue label;
				}
			}
		}
		return new HashSet<OptimLinearConstraint>(cons);
	}

}
