/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import framework.model.ConstraintVisitor;
import framework.model.OptimLinearConstraint;
import framework.model.OptimVariable;

/**
 * General implementation of a linear constraint.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPLinearConstraint extends OptimLinearConstraint {

	/**
	 * Coefficients
	 */
	private final double[] coeffs;

	/**
	 * Map each variable to the corresponding coefficient
	 */
	private final Map<OptimVariable, Double> varcoeffs = new HashMap<OptimVariable, Double>();

	/**
	 * All variables with explicitly stated coefficient
	 */
	private final OptimVariable[] vars;

	/**
	 * Construct a new LPLinearConstraint
	 * 
	 * @param coefficients
	 *            Coefficients
	 * @param variables
	 *            Variables
	 * @param constraintType
	 *            Relationship between the left-hand and the right-hand side
	 * @param rightHandSide
	 *            Constant value on the right-hand side of the constraint
	 */
	public LPLinearConstraint(double[] coefficients, OptimVariable[] variables, int constraintType, double rightHandSide) {
		coeffs = coefficients;
		vars = variables;
		if (coeffs.length != variables.length) {
			throw new RuntimeException("Coefficients and variables are unbalanced");
		}
		for (int i = 0; i < coeffs.length; ++i) {
			varcoeffs.put(vars[i], coeffs[i]);
		}
		type = constraintType;
		rhs = rightHandSide;
	}

	/**
	 * Get the coefficient associated with the variable
	 * 
	 * @param var
	 *            Variable
	 * @return The associated coefficient or zero if none was stated.
	 */
	@Override
	public double getCoefficient(OptimVariable var) {
		if (varcoeffs.containsKey(var)) {
			return varcoeffs.get(var);
		}
		return 0.0;
	}

	/**
	 * Get all variables with explicitly stated coefficient
	 * 
	 * @return Variables
	 */
	@Override
	public Set<OptimVariable> getVariables() {
		return new HashSet<OptimVariable>(Arrays.asList(vars));
	}

	@Override
	public void accept(ConstraintVisitor visitor) {
		visitor.visitLPLinearConstraint(this);
	}

}
