/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.model;

import framework.model.OptimVariable;

/**
 * Representation of a variable for linear problems
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPVariable extends OptimVariable {

	/**
	 * Construct new variable with given name
	 * 
	 * @param name Name
	 */
	public LPVariable(String name) {
		super(name);
	}

	/**
	 * Construct new variable
	 */
	public LPVariable() {
		super();
	}

	/**
	 * If true, the variable is only allowed to have binary (0/1) values;
	 */
	protected boolean onlyBinary;

	/**
	 * Is variable restricted to binary-only values
	 * 
	 * @return True if variable is restricted to binary-only values
	 */
	public boolean isOnlyBinary() {
		return onlyBinary;
	}

	/**
	 * Set/Reset restriction to binary-only values
	 * 
	 * @param onlyBinary True if variable should be restricted to binary-only values
	 */
	public void setOnlyBinary(boolean onlyBinary) {
		this.onlyBinary = onlyBinary;
	}

	/**
	 * Is variable restricted to integer-only values
	 * 
	 * @return True if variable is restricted to integer-only values
	 */
	public boolean isOnlyInteger() {
		return onlyInteger;
	}

	/**
	 * Set/Reset restriction to integer-only values
	 * 
	 * @param onlyInteger True if variable should be restricted to integer-only values
	 */
	public void setOnlyInteger(boolean onlyInteger) {
		this.onlyInteger = onlyInteger;
	}

	/**
	 * If true, the variable is only allowed to have integer values;
	 */
	protected boolean onlyInteger;

}
