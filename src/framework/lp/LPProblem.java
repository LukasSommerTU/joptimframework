/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp;

import framework.Solveable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearVariableConstraint;

/**
 * Interface for linear problems
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public interface LPProblem extends Solveable {

	/**
	 * Add constraint to the problem
	 * 
	 * @param addedConstraint Constraint to be added
	 */
	public void addConstraint(OptimLinearConstraint addedConstraint);

	/**
	 * Remove constraint from the problem
	 * 
	 * @param removedConstraint Constraint to be removed
	 * @return {@literal true} if the constraint was present and was removed
	 *         successfully.
	 */
	public boolean removeConstraint(OptimLinearConstraint removedConstraint);

	/**
	 * Add variable constraint (upper and lower bound)
	 * 
	 * @param constraint Lower and upper bound to a variable
	 */
	public void addVariableConstraint(OptimLinearVariableConstraint constraint);
}
