/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.solver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.ColtMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;
import com.winvector.lp.LPSoln;
import com.winvector.lp.impl.RevisedSimplexSolver;

import framework.Solveable;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;
import framework.sdc.feasibility_checker.model.util.Pair;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;

/**
 * Solver using the Win-Vector Linear Program Solver. See:
 * http://www.win-vector.
 * com/blog/2012/11/yet-another-java-linear-programming-library/ for further
 * information.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class WVLPSolver extends LPSolver {

	/**
	 * Constraint matrix
	 */
	private Matrix<ColtMatrix> A;

	/**
	 * RHS-vector
	 */
	private double[] b;

	/**
	 * Objective-function coefficients
	 */
	private double[] c;

	/**
	 * Map each variable to its index in matrix rows
	 */
	private final Map<OptimVariable, Integer> varindices = new HashMap<OptimVariable, Integer>();

	/**
	 * Map constraints to the eventually introduced slack-variables. Slack
	 * variables are introduced to express inequalities as equalities
	 */
	private final Map<OptimLinearConstraint, OptimVariable> slackVars = new HashMap<OptimLinearConstraint, OptimVariable>();

	/**
	 * 
	 */
	private final Map<OptimLinearVariableConstraint, Pair<OptimVariable, OptimVariable>> varConsSlack = new HashMap<OptimLinearVariableConstraint, Pair<OptimVariable, OptimVariable>>();

	/**
	 * All variables from the original linear problem, slack variables are not
	 * included
	 */
	private final Set<OptimVariable> variables = new HashSet<OptimVariable>();

	/**
	 * Number of variables, includes slack variables
	 */
	private int numVars;

	/**
	 * Solution vector
	 */
	private double[] solution;

	int numCons;

	/**
	 * Current row index
	 */
	int count;

	Solveable system;

	/**
	 * Solve the given linear problem using {@link RevisedSimplexSolver}.
	 * 
	 * @param system Linear problem
	 * @return {@literal true} if the system was solved successfully
	 */
	@Override
	public boolean solve(Solveable system) {
		// if (!system.isRestrictedNonNegative()) {
		// throw new
		// RuntimeException("This solver does only support non-negativity constraints!");
		// }
		// TODO
		this.system = system;
		variables.clear();
		slackVars.clear();
		varConsSlack.clear();
		varindices.clear();
		introduceSlackVariables(this.system);
		variables.addAll(this.system.getVariables());
		numVars += variables.size();
		A = ColtMatrix.factory.newMatrix(numCons, numVars, false);
		b = new double[numCons];
		c = new double[numVars];
		solution = new double[numVars];
		count = 0;
		getVariableSpecificData(system);
		if(!getConstraints(system)){
			return false;
		}
		try {
			LPEQProb prob = new LPEQProb(A.columnMatrix(), b, new DenseVec(c));
			if (isDebugOutputEnabled()) {
				prob.print();
			}
			final LPSoln soln = prob.solveDebug(new RevisedSimplexSolver(), 1.0e-5, Integer.MAX_VALUE, ColtMatrix.factory);
			soln.primalSolution.toArray(solution);
			setVariableSolution(solution);
		} catch (LPMalformedException e) {
			e.printStackTrace();
			return false;
		} catch (LPException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Associate each variable with its optimal solution
	 * 
	 * @param solution Solution vector
	 */
	private void setVariableSolution(double[] solution) {
		// for (Pair<OptimVariable, OptimVariable> p : varConsSlack.values()) {
		// System.out.println(p.getFirst() + " " +
		// solution[varindices.get(p.getFirst())]);
		// System.out.println(p.getSecond() + " " +
		// solution[varindices.get(p.getSecond())]);
		// }
		// for (OptimVariable var : slackVars.values()) {
		// System.out.println(var + " " + solution[varindices.get(var)]);
		// var.setOptimalSolution(solution[varindices.get(var)]);
		// }
		for (OptimVariable var : variables) {
			// System.out.println(var + " " + solution[varindices.get(var)]);
			var.setOptimalSolution(solution[varindices.get(var)]);
		}
	}

	/**
	 * Extract the constraints from the problem
	 * 
	 * @param system Linear problem
	 */
	private boolean getConstraints(Solveable system) {
		Set<OptimLinearConstraint> constraints = system.removeDuplicates();
		if(constraints == null){
			return false;
		}
		for (OptimLinearConstraint cons : constraints) {
			cons.accept(this);
		}
		return true;
	}

	/**
	 * Get coefficient in objective function for each variable. This solver does
	 * not support other boundaries than non-negativity constraints
	 * 
	 * @param system Linear problem
	 */
	private void getVariableSpecificData(Solveable system) {
		OptimLinearObjectiveFunction objfun = system.getObjectiveFunction();
		if (objfun == null) {
			throw new RuntimeException("No objective function specified!");
		}
		int index = 0;

		for (OptimVariable slackVar : slackVars.values()) {
			varindices.put(slackVar, index);
			++index;
		}

		for (Pair<OptimVariable, OptimVariable> p : varConsSlack.values()) {
			varindices.put(p.getFirst(), index);
			++index;
			varindices.put(p.getSecond(), index);
			++index;
		}

		int factor = 1;
		if (objfun.getOptimType() == OptimLinearObjectiveFunction.MAX) {
			factor = -1;
		}
		for (OptimVariable var : variables) {
			if (var instanceof LPVariable) {
				if (((LPVariable) var).isOnlyBinary() || ((LPVariable) var).isOnlyInteger()) {
					throw new RuntimeException("WVLPSolver cannot be used to solve ILP/MILP-problems. Use other solver instead! (e.g. LPSolveSolver)");
				}
			}
			varindices.put(var, index);
			c[index] = (objfun.getVarCoefficient(var) * factor);
			OptimLinearVariableConstraint varCons = system.getVariableConstraint(var);
			if (varCons != null) {
				int varindex = varindices.get(var);
				if (varCons.getLowerBound() != varCons.getUpperBound()) {
					int slackLB = varindices.get(varConsSlack.get(varCons).getFirst());
					int slackUB = varindices.get(varConsSlack.get(varCons).getSecond());
					// Greater-equal constraint for the lower bound
					A.set(count, varindex, -1);
					A.set(count, slackLB, 1);
					b[count] = varCons.getLowerBound() * -1;
					++count;
					// Less-equal constraint for the upper bound
					A.set(count, varindex, 1);
					A.set(count, slackUB, 1);
					b[count] = varCons.getUpperBound();
					++count;
				} else {
					// Equality constraint
					A.set(count, varindex, 1);
					b[count] = varCons.getLowerBound();
					++count;
				}
			}
			++index;
		}

	}

	/**
	 * Introduce slack-variables for all inequalities in the linear problem
	 * 
	 * @param system Linear problem
	 */
	private void introduceSlackVariables(Solveable system) {
		for (OptimLinearConstraint cons : system.getConstraints()) {
			if (cons.getType() != OptimLinearConstraint.EQUALS) {
				OptimVariable slack = new OptimVariable("slack" + cons.hashCode());
				slackVars.put(cons, slack);
				++numVars;
			}
			numCons++;
		}
		for (OptimLinearVariableConstraint cons : system.getVariableConstraints()) {
			if (cons.getLowerBound() != cons.getUpperBound()) {
				OptimVariable slackLB = new OptimVariable("LB_slack_" + cons.getVar().toString());
				OptimVariable slackUB = new OptimVariable("UB_slack" + cons.getVar().toString());
				varConsSlack.put(cons, new Pair<OptimVariable, OptimVariable>(slackLB, slackUB));
				/*
				 * To set lower bound and upper bound with different values, two
				 * constraints and slack-variables are necessary
				 */
				numVars += 2;
				numCons += 2;
			} else {
				numCons++;
			}

		}

	}

	/**
	 * Get the optimal value of the objective function
	 * 
	 * @return Optimal value of the objective function
	 */
	@Override
	public double getValueObjectiveFunction() {
		if (solution == null) {
			System.err.println("Solve system first!");
			return Double.NaN;
		}
		int factor = 1;
		if (system.getObjectiveFunction().getOptimType() == OptimLinearObjectiveFunction.MAX) {
			factor = -1;
		}
		double objVal = 0;
		for (int i = 0; i < c.length; ++i) {
			objVal += c[i] * solution[i] * factor;
		}
		return objVal;
	}

	@Override
	public void visitLPLinearConstraint(LPLinearConstraint cons) {
		visitConstraint(cons);
	}

	@Override
	public void visitSDCLinearConstraint(SDCLinearConstraint cons) {
		visitConstraint(cons);
	}

	@Override
	public void visitSDCSingleVariableConstraint(SDCSingleVariableConstraint cons) {
		visitConstraint(cons);
	}

	private void visitConstraint(OptimLinearConstraint cons) {
		int factor = 1;
		if (cons.getType() == OptimLinearConstraint.GREATER_EQUAL) {
			factor = -1;
		} else {
			factor = 1;
		}
		for (OptimVariable var : cons.getVariables()) {
			A.set(count, varindices.get(var), (cons.getCoefficient(var) * factor));
		}
		b[count] = (cons.getRHS() * factor);
		if (cons.getType() != OptimLinearConstraint.EQUALS) {
			A.set(count, varindices.get(slackVars.get(cons)), 1);
		}
		++count;
	}
}
