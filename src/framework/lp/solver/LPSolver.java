/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.solver;

import framework.Solveable;
import framework.model.ConstraintVisitor;

/**
 * Interface for access to a lp-solver lib. To provide a interface to a lp-lib
 * implement this interface.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public abstract class LPSolver implements ConstraintVisitor {

	private boolean debugEnabled = false;

	/**
	 * Solve the given linear problem
	 * 
	 * @param system The linear problem to be solved
	 * @return true if the given lp-problem was solved successfully, false
	 *         otherwise
	 */
	public abstract boolean solve(Solveable system);

	/**
	 * Toogle the debug output
	 * 
	 * @param enable {@literal true} if the debug-output should be enable, false
	 *            otherwise
	 */
	public void toggleDebugOutput(boolean enable) {
		debugEnabled = enable;
	}

	/**
	 * Is the debug output enabled
	 * 
	 * @return True if the output is enabled, false otherwise
	 */
	public boolean isDebugOutputEnabled() {
		return debugEnabled;
	}

	/**
	 * Get the optimal value of the objective function
	 * 
	 * @return Optimal value of the objective function
	 */
	public abstract double getValueObjectiveFunction();
}
