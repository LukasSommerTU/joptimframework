/*******************************************************************************
 * Copyright 2016 Julian Oppermann, Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.solver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import framework.Solveable;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;
import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Param;
import ilog.cplex.IloCplex.UnknownObjectException;

/**
 * LP-solver based on CPLEX.
 * 
 * @author Julian Oppermann <oppermann@esa.informatik.tu-darmstadt.de>
 *
 */
public class CPLEXSolver extends LPSolver {
	/**
	 * CPLEX solver instance
	 */
	private IloCplex cp;

	/**
	 * Maps each framework variable to the corresponding CPLEX variable.
	 */
	private Map<OptimVariable, IloNumVar> vars;

	/**
	 * Value of the objective function
	 */
	private double objValue;

	/**
	 * Solve the given linear problem using CPLEX.
	 * 
	 * @param system Linear problem
	 * @return {@literal true} if the system was solved successfully
	 */
	@Override
	public boolean solve(Solveable system) {
		vars = new HashMap<>();
		objValue = 0.0;
		
		boolean feasible;
		try {
			cp = new IloCplex();
			/*
			 * Enable/Disable output
			 */
			if(!this.isDebugOutputEnabled()){
				cp.setOut(null);
			}
			else{
				cp.setOut(System.out);
			}
			constructVariables(system);
			constructObjectiveFunction(system);
			constructConstraints(system);
			
			cp.setParam(Param.Threads, 1); // force single-thread mode
			feasible = cp.solve();
			if (feasible) {
				setSolution();
				objValue = cp.getObjValue();
			}
			cp.end();
		}
		catch (IloException e) {
			throw new RuntimeException(e);
		}
		
		return feasible;
	}

	/* (non-Javadoc)
	 * @see framework.lp.solver.LPSolver#getValueObjectiveFunction()
	 */
	@Override
	public double getValueObjectiveFunction() {
		return objValue;
	}

	/**
	 * Write current problem to a file in CPLEX lp-format.
	 * 
	 * @param filename File
	 */
	public void writeCurrentProblemCPLEX(String filename) {
		try {
			cp.exportModel(filename);
		}
		catch(IloException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Forward solution values from the CPLEX variables to the framework variables.
	 * 
	 * @throws IloException
	 */
	private void setSolution() throws IloException {
		for (OptimVariable var : vars.keySet()) {
			IloNumVar cpvar = vars.get(var);
			try {
				var.setOptimalSolution(cp.getValue(cpvar));
			}
			catch (UnknownObjectException e) {
				// ignore, variable was "optimised" away
			}
		}
	}

	/**
	 * Construct variables and set their bounds.
	 * 
	 * @param system Linear problem
	 * @throws IloException
	 */
	private void constructVariables(Solveable system) throws IloException {
		for (OptimVariable var : system.getVariables()) {
			double lb = Double.NEGATIVE_INFINITY, ub = Double.POSITIVE_INFINITY;
			
			OptimLinearVariableConstraint cons = system.getVariableConstraint(var);
			if (cons != null) {
				lb = cons.getLowerBound();
				ub = cons.getUpperBound();
			}
			
			IloNumVarType type = IloNumVarType.Float;
			if (var instanceof LPVariable) {
				if (((LPVariable) var).isOnlyBinary()) {
					type = IloNumVarType.Bool;
					lb = 0; ub = 1;
				}
				else if (((LPVariable) var).isOnlyInteger()) {
					type = IloNumVarType.Int;
				}
			}
			
			if (system.isRestrictedNonNegative()) {
				lb = Math.max(0.0, lb);
				ub = Math.max(0.0, ub);
			}
			
			vars.put(var, cp.numVar(lb, ub, type, var.getName()));
		}
	}

	/**
	 * Construct the objective function.
	 * 
	 * @param system Linear problem
	 * @throws IloException
	 */
	private void constructObjectiveFunction(Solveable system) throws IloException {
		OptimLinearObjectiveFunction obj = system.getObjectiveFunction();
		
		List<IloNumExpr> exprs = new ArrayList<>();
		for (OptimVariable var : vars.keySet()) {
			double coeff = obj.getVarCoefficient(var);
			if (coeff == 0.0)
				continue;
			
			IloNumVar cpvar = vars.get(var);
			exprs.add(cp.prod(coeff, cpvar));
		}
		
		double linPart = obj.getLinearPart();
		if (linPart != 0.0)
			exprs.add(cp.constant(linPart));
		
		IloNumExpr sum = cp.sum(exprs.toArray(new IloNumExpr[exprs.size()]));
		if (obj.getOptimType() == OptimLinearObjectiveFunction.MIN)
			cp.addMinimize(sum);
		else if (obj.getOptimType() == OptimLinearObjectiveFunction.MAX)
			cp.addMaximize(sum);
		else
			throw new RuntimeException("Unsupported optimisation type");
	}

	/**
	 * Visit all constraints.
	 * 
	 * @param system Linear problem
	 */
	private void constructConstraints(Solveable system) {
		for (OptimLinearConstraint cons : system.getConstraints())
			cons.accept(this);
	}

	/**
	 * Add the given constraint to the model.
	 * 
	 * @param cons A linear constraint
	 */
	private void constructConstraint(OptimLinearConstraint cons) {
		try {
			List<IloNumExpr> exprs = new ArrayList<>();
			for (OptimVariable var : cons.getVariables()) {
				double coeff = cons.getCoefficient(var);
				if (coeff == 0.0)
					continue;
				
				IloNumVar cpvar = vars.get(var);
				exprs.add(cp.prod(coeff, cpvar));
			}
			
			IloNumExpr sum = cp.sum(exprs.toArray(new IloNumExpr[exprs.size()]));
			double rhs = cons.getRHS();
			switch (cons.getType()) {
			case OptimLinearConstraint.LESS_EQUAL:    cp.addLe(sum, rhs); break;
			case OptimLinearConstraint.EQUALS:        cp.addEq(sum, rhs); break;
			case OptimLinearConstraint.GREATER_EQUAL: cp.addGe(sum, rhs); break;
			default: throw new RuntimeException("Unsupported constraint trype");
			}
		}
		catch (IloException e) {
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see framework.model.ConstraintVisitor#visitLPLinearConstraint(framework.lp.model.LPLinearConstraint)
	 */
	@Override
	public void visitLPLinearConstraint(LPLinearConstraint cons) {
		constructConstraint(cons);
	}

	/* (non-Javadoc)
	 * @see framework.model.ConstraintVisitor#visitSDCLinearConstraint(framework.sdc.model.SDCLinearConstraint)
	 */
	@Override
	public void visitSDCLinearConstraint(SDCLinearConstraint cons) {
		constructConstraint(cons);
	}

	/* (non-Javadoc)
	 * @see framework.model.ConstraintVisitor#visitSDCSingleVariableConstraint(framework.sdc.model.SDCSingleVariableConstraint)
	 */
	@Override
	public void visitSDCSingleVariableConstraint(SDCSingleVariableConstraint cons) {
		constructConstraint(cons);
	}
}
