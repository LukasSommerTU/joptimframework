/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.solver;

import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import com.winvector.lp.impl.RevisedSimplexSolver;

import framework.Solveable;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;

/**
 * LP-solver based on lp_solve.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPSolveSolver extends LPSolver {

	/**
	 * Map each variable to its index in matrix rows
	 */
	private final Map<OptimVariable, Integer> varindices = new HashMap<OptimVariable, Integer>();

	/**
	 * All variables from the original linear problem, slack variables are not included
	 */
	private final Set<OptimVariable> variables = new HashSet<OptimVariable>();
	
	private Map<OptimVariable, Double> lowerBounds = new HashMap<OptimVariable, Double>();
	private Map<OptimVariable, Double> upperBounds = new HashMap<OptimVariable, Double>();


	/**
	 * Number of variables, includes slack variables
	 */
	private int numVars;

	/**
	 * Solution vector
	 */
	private double[] solution;

	/**
	 * LPSolve-instance
	 */
	private LpSolve solver;

	/**
	 * Value of the objective function
	 */
	private double objVal;

	/**
	 * Pivoting rule, for possible values see: http://lpsolve.sourceforge.net/5.5/set_pivoting.htm
	 */
	private int pivotingRule = 34;

	/**
	 * If true, dump problem to CPLEX-format
	 */
	private boolean dumpProblem = false;

	/**
	 * Output-file for dump to CPLEX-format
	 */
	private String outputFile;

	/**
	 * Solve the given linear problem using {@link RevisedSimplexSolver}.
	 * 
	 * @param system Linear problem
	 * @return {@literal true} if the system was solved successfully
	 */
	@Override
	public boolean solve(Solveable system) {
		variables.addAll(system.getVariables());
		numVars = variables.size();
		lowerBounds.clear();
		upperBounds.clear();
		int index = 1;
		for (OptimVariable var : variables) {
			varindices.put(var, index);
			index++;
		}
		try {
			solver = LpSolve.makeLp(0, numVars);
			solver.setVerbose(0);
			solver.setPivoting(pivotingRule);
			if(!getConstraints(system)){
				return false;
			}
			getVariableSpecificData(system);
			if (dumpProblem) {
				writeCurrentProblemCPLEX(outputFile);
			}
			int returnCode = solver.solve(); // TODO
			if (returnCode != LpSolve.OPTIMAL) {
				return false;
			}
			solution = solver.getPtrVariables();
			setVariableSolution(solution);
			objVal = solver.getObjective();
			solver.deleteLp();
			return true;
		} catch (LpSolveException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Associate each variable with its optimal solution
	 * 
	 * @param solution Solution vector
	 */
	private void setVariableSolution(double[] solution) {
		for (OptimVariable var : variables) {
			var.setOptimalSolution(solution[varindices.get(var) - 1]);
		}
	}

	/**
	 * Extract the constraints from the problem
	 * 
	 * @param system Linear problem
	 * @throws LpSolveException
	 */
	private boolean getConstraints(Solveable system) throws LpSolveException {
//		Set<OptimLinearConstraint> constraints = system.removeDuplicates();
		@SuppressWarnings("unchecked")
		Set<OptimLinearConstraint> constraints = (Set<OptimLinearConstraint>) system.getConstraints();
		if(constraints == null){
			return false;
		}
		for (OptimLinearConstraint cons : constraints) {
			cons.accept(this);
		}
		for(OptimVariable var : lowerBounds.keySet()){
			int index = varindices.get(var);
			double lowerBound = lowerBounds.get(var);
			double upperBound = upperBounds.get(var);
			try {
				solver.setBounds(index, lowerBound, upperBound);
			} catch (LpSolveException e) {
				throw new RuntimeException(e);
			}
		}
		return true;
	}

	/**
	 * Get coefficient in objective function for each variable. This solver does not support other boundaries than non-negativity constraints
	 * 
	 * @param system Linear problem
	 * @throws LpSolveException
	 */
	private void getVariableSpecificData(Solveable system) throws LpSolveException {
		OptimLinearObjectiveFunction objfun = system.getObjectiveFunction();
		double[] row = new double[numVars + 1];
		for (OptimVariable var : variables) {
			int index = varindices.get(var);
			row[index] = objfun.getVarCoefficient(var);
			if (!system.isRestrictedNonNegative()) {
				OptimLinearVariableConstraint varcons = system.getVariableConstraint(var);
				if (varcons != null) {
					solver.setBounds(index, varcons.getLowerBound(), varcons.getUpperBound());
				}
			}
			if (var instanceof LPVariable) {
				if (((LPVariable) var).isOnlyBinary()) {
					solver.setBinary(index, true);
				} else if (((LPVariable) var).isOnlyInteger()) {
					solver.setInt(index, true);
				}
			}
		}
		solver.setObjFn(row);
		if (objfun.getOptimType() == OptimLinearObjectiveFunction.MAX) {
			solver.setMaxim();
		}
		// solver.strSetObjFn(convertRow2String(row));
	}

	@Override
	public double getValueObjectiveFunction() {
		return objVal;
	}

	/**
	 * Build string from double array and replace decimal separator accordingly
	 * 
	 * @param row Double-array representing row
	 * @return String with correct decimal separator
	 */
	@SuppressWarnings("unused")
	private String convertRow2String(double[] row) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < row.length; ++i) {
			sb.append(row[i]);
			if (i < row.length - 1) {
				sb.append(" ");
			}
		}
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
		return sb.toString().replaceAll("\\.", String.valueOf(dfs.getDecimalSeparator()));
	}

	/**
	 * Set the pivoting rule for lp_solve
	 * 
	 * @param rule For possible values see: http://lpsolve.sourceforge.net/5.5/set_pivoting.htm
	 */
	public void setPivotingRule(int rule) {
		pivotingRule = rule;
	}

	/**
	 * Write current problem to a file in CPLEX lp-format
	 * 
	 * @param filename File
	 */
	public void writeCurrentProblemCPLEX(String filename) {
		try {
			solver.writeLp(filename + ".lp");
			solver.setXLI("xli_CPLEX");
			solver.writeXLI(filename, "", true);
		} catch (LpSolveException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Should the problem be dumped to file
	 * @return True if the problem should be dumped to file
	 */
	public boolean isDumpProblem() {
		return dumpProblem;
	}

	/**
	 * Set/Reset dumping of problem to file
	 * @param dumpProblem True if the problem should be dumped to file
	 */
	public void setDumpProblem(boolean dumpProblem) {
		this.dumpProblem = dumpProblem;
	}

	/**
	 * Output file for dump
	 * @return Filename of the dump file
	 */
	public String getOutputFile() {
		return outputFile;
	}

	/**
	 * Set filename of the dump file
	 * @param outputFile Filename of the dump file
	 */
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	@Override
	public void visitLPLinearConstraint(LPLinearConstraint cons) {
		visitConstraint(cons);
	}

	@Override
	public void visitSDCLinearConstraint(SDCLinearConstraint cons) {
		visitConstraint(cons);
	}

	private void visitConstraint(OptimLinearConstraint cons) {
		double[] row = new double[numVars + 1];
		for (OptimVariable var : cons.getVariables()) {
			int index = varindices.get(var);
			row[index] = cons.getCoefficient(var);
		}
		int type;
		switch (cons.getType()) {
			case OptimLinearConstraint.GREATER_EQUAL:
				type = LpSolve.GE;
				break;
			case OptimLinearConstraint.EQUALS:
				type = LpSolve.EQ;
				break;
			case OptimLinearConstraint.LESS_EQUAL:
				type = LpSolve.LE;
				break;
			default:
				type = LpSolve.LE;
		}

		try {
			solver.addConstraint(row, type, cons.getRHS());
		} catch (LpSolveException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void visitSDCSingleVariableConstraint(SDCSingleVariableConstraint cons) {
		OptimVariable var = cons.getVariable();
		int type = cons.getType();
		double upperBound = Double.POSITIVE_INFINITY;
		double lowerBound = Double.NEGATIVE_INFINITY;
		if (type == OptimLinearConstraint.LESS_EQUAL || type == OptimLinearConstraint.EQUALS) {
			upperBound = cons.getRHS();
		}
		if (type == OptimLinearConstraint.GREATER_EQUAL || type == OptimLinearConstraint.EQUALS) {
			lowerBound = cons.getRHS();
		}
		if(lowerBounds.containsKey(var)){
			double lowerOld = lowerBounds.get(var);
			double upperOld = upperBounds.get(var);
			if(lowerBound > lowerOld){
				lowerBounds.put(var, lowerBound);
			}
			if(upperBound < upperOld){
				upperBounds.put(var, upperBound);
			}
		}
		else{
			lowerBounds.put(var, lowerBound);
			upperBounds.put(var, upperBound);
		}
	}
}
