/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package framework.lp.solver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.NoFeasibleSolutionException;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.linear.SolutionCallback;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

import framework.Solveable;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;

/**
 * LP-solver implementation based on the SimplexSolver from 
 * Apache's Commons Math Library {@link org.apache.commons.math3.optim.linear.SimplexSolver}
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
/**
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class ApacheSimplexSolver extends LPSolver {

	/**
	 * Objective function
	 */
	private LinearObjectiveFunction objectiveFunction;

	/**
	 * Linear constraints
	 */
	private final Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();

	/**
	 * All involved variables
	 */
	private final Set<OptimVariable> variables = new HashSet<OptimVariable>();

	/**
	 * Map each variable to its index in the matrix-rows.
	 */
	private final Map<OptimVariable, Integer> varindices = new HashMap<OptimVariable, Integer>();

	/**
	 * Number of variables
	 */
	private int numVars;

	/**
	 * Is the system restricted to include only non-negative values
	 */
	private boolean nonNegative;

	/**
	 * Optimal value of the objective function
	 */
	private double objFunSolution = Double.NaN;

	/**
	 * Solve the given linear problem
	 * 
	 * @param system Linear problem
	 * @return {@literal true} if the system was solved successfully
	 */
	@Override
	public boolean solve(Solveable system) {
		constraints.clear();
		variables.addAll(system.getVariables());
		numVars = variables.size();
		nonNegative = system.isRestrictedNonNegative();
		getVariableSpecificData(system);
		getConstraints(system);
		GoalType goal;
		if (system.getObjectiveFunction().getOptimType() == OptimLinearObjectiveFunction.MAX) {
			goal = GoalType.MAXIMIZE;
		} else if (system.getObjectiveFunction().getOptimType() == OptimLinearObjectiveFunction.MIN) {
			goal = GoalType.MINIMIZE;
		} else {
			throw new RuntimeException("Unkown goal type!");
		}
		LinearConstraintSet constraintSet = new LinearConstraintSet(constraints);
		System.out.println("Finished translation, starting to solve LP");
		try {
			SolutionCallback sc = new SolutionCallback();
			new SimplexSolver(1e-3).optimize(objectiveFunction, constraintSet, goal, sc);
			if (!sc.isSolutionOptimal()) {
				return false;
			}
			PointValuePair solution = sc.getSolution();
			setVariableSolution(solution.getPoint());
			objFunSolution = solution.getValue();
			return true;
		} catch (NoFeasibleSolutionException e) {
			return false;
		}

	}

	/**
	 * Associate every variable with its optimal solution
	 * 
	 * @param solution Array with optimal solutions
	 */
	private void setVariableSolution(double[] solution) {
		for (OptimVariable var : variables) {
			var.setOptimalSolution(solution[varindices.get(var)]);
		}
	}

	/**
	 * Extract all constraints from the linear problem
	 * 
	 * @param system Linear problem
	 */
	private void getConstraints(Solveable system) {
		for (OptimLinearConstraint cons : system.getConstraints()) {
			cons.accept(this);
		}
	}

	/**
	 * Extract the coefficient in the objective function and boundaries for
	 * every variable
	 * 
	 * @param system Linear problem
	 */
	private void getVariableSpecificData(Solveable system) {
		OptimLinearObjectiveFunction objfun = system.getObjectiveFunction();
		if (objfun == null) {
			throw new RuntimeException("No objective function specified!");
		}
		int index = 0;
		double[] d = new double[numVars];
		for (OptimVariable var : variables) {
			if (var instanceof LPVariable) {
				if (((LPVariable) var).isOnlyBinary() || ((LPVariable) var).isOnlyInteger()) {
					throw new RuntimeException("ApacheSimplexSolver cannot be used to solve ILP/MILP-problems. Use other solver instead! (e.g. LPSolveSolver)");
				}
			}
			varindices.put(var, index);
			d[index] = objfun.getVarCoefficient(var);
			double[] onehot = new double[numVars];
			onehot[index] = 1;
			if (nonNegative) {
				constraints.add(new LinearConstraint(onehot, Relationship.GEQ, 0));
				constraints.add(new LinearConstraint(onehot, Relationship.LEQ, Double.MAX_VALUE));
			} else {
				OptimLinearVariableConstraint varcons = system.getVariableConstraint(var);
				if (varcons != null) {
					constraints.add(new LinearConstraint(onehot, Relationship.GEQ, varcons.getLowerBound()));
					constraints.add(new LinearConstraint(onehot, Relationship.LEQ, varcons.getUpperBound()));
				}
			}
			++index;
		}
		objectiveFunction = new LinearObjectiveFunction(d, objfun.getLinearPart());
	}

	/**
	 * Get the optimal solution for the objective function
	 * 
	 * @return Optimal solution for objective function
	 */
	@Override
	public double getValueObjectiveFunction() {
		if (Double.isNaN(objFunSolution)) {
			System.err.println("Solve system first!");
		}
		return objFunSolution;
	}

	@Override
	public void visitLPLinearConstraint(LPLinearConstraint cons) {
		visitConstraint(cons);
	}

	@Override
	public void visitSDCLinearConstraint(SDCLinearConstraint cons) {
		visitConstraint(cons);
	}

	private void visitConstraint(OptimLinearConstraint cons) {
		double[] row = new double[numVars];
		for (OptimVariable var : cons.getVariables()) {
			row[varindices.get(var)] = cons.getCoefficient(var);
		}
		Relationship relation;
		switch (cons.getType()) {
			case OptimLinearConstraint.LESS_EQUAL:
				relation = Relationship.LEQ;
				break;
			case OptimLinearConstraint.EQUALS:
				relation = Relationship.EQ;
				break;
			case OptimLinearConstraint.GREATER_EQUAL:
				relation = Relationship.GEQ;
				break;
			default:
				throw new RuntimeException("Unknown constraint type!");
		}
		constraints.add(new LinearConstraint(row, relation, cons.getRHS()));
	}

	@Override
	public void visitSDCSingleVariableConstraint(SDCSingleVariableConstraint cons) {
		double[] onehot = new double[numVars];
		onehot[varindices.get(cons.getVariable())] = 1;
		int type = cons.getType();
		if (type == OptimLinearConstraint.LESS_EQUAL) {
			constraints.add(new LinearConstraint(onehot, Relationship.LEQ, cons.getRHS()));
		}
		if (type == OptimLinearConstraint.GREATER_EQUAL) {
			constraints.add(new LinearConstraint(onehot, Relationship.GEQ, cons.getRHS()));
		}
		if (type == OptimLinearConstraint.EQUALS) {
			constraints.add(new LinearConstraint(onehot, Relationship.EQ, cons.getRHS()));
		}
	}

}
