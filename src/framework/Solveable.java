package framework;

import java.util.Set;

import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.model.OptimVariable;

/**
 * Interface for optimization problems that can be passed to a LP-solver
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public interface Solveable {

	/**
	 * Set {@link LPSolver} to use
	 * 
	 * @param solver {@link LPSolver}
	 */
	public void setSolver(LPSolver solver);

	/**
	 * Get the solver currently set
	 * 
	 * @return The set {@link LPSolver}
	 */
	public LPSolver getSolver();

	/**
	 * Set objective function
	 * 
	 * @param objfunc Objective function
	 */
	public void setObjectiveFunction(OptimLinearObjectiveFunction objfunc);

	/**
	 * Get the objective function currently set
	 * 
	 * @return objective function
	 */
	public OptimLinearObjectiveFunction getObjectiveFunction();

	/**
	 * Get all constraints
	 * 
	 * @return Set of all constraints
	 */
	public Set<? extends OptimLinearConstraint> getConstraints();

	/**
	 * Get all variables included in the linear problem
	 * 
	 * @return Set of all variables
	 */
	public Set<? extends OptimVariable> getVariables();

	/**
	 * Get lower and upper bound for the given variable
	 * 
	 * @param var Variable
	 * @return Lower and upper bound for the variable
	 */
	public OptimLinearVariableConstraint getVariableConstraint(OptimVariable var);

	/**
	 * Get all variable constraints
	 * 
	 * @return Set of all variable constraints
	 */
	public Set<? extends OptimLinearVariableConstraint> getVariableConstraints();

	/**
	 * Is the linear problem restricted to be non-negative
	 * 
	 * @return {@literal true} if the system has only non-negativity constraints
	 */
	public boolean isRestrictedNonNegative();

	/**
	 * Set if the system is restricted to be non-negative
	 * 
	 * @param restrictedNonNegative Boolean
	 */
	public void setRestrictedNonNegative(boolean restrictedNonNegative);

	/**
	 * Solve the linear problem
	 * 
	 * @return {@literal true} if the system was solved successfully
	 */
	public boolean solve();

	/**
	 * Is the optimal solution for each variable integral?
	 * 
	 * @return {@literal true} if all variables have a integer optimal solution
	 */
	public boolean hasIntegralOptimalSolution();
	
	public Set<OptimLinearConstraint> removeDuplicates();
}
