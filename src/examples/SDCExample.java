/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package examples;

import framework.sdc.SDC;
import framework.sdc.feasibility_checker.BellmanFordChecker;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;

/**
 * Small example of a SDC to demonstrate the use of the framework.
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class SDCExample {

	public static void main(String[] args) {
		// Start new SDC
		SDC sdc = new SDC();
		// Generate variables
		SDCVariable var1 = new SDCVariable("x1");
		SDCVariable var2 = new SDCVariable("x2");
		SDCVariable var3 = new SDCVariable("x3");
		SDCVariable var4 = new SDCVariable("x4");
		SDCVariable var5= new SDCVariable("x5");
		
		//Build constraints
		// x1 - x2 =< 3
		SDCLinearConstraint cons1 = new SDCLinearConstraint(var1,var2, 3, SDCLinearConstraint.LESS_EQUAL);
		// x3 - x2 =< -2
		SDCLinearConstraint cons2 = new SDCLinearConstraint(var3, var2, -2, SDCLinearConstraint.LESS_EQUAL);
		// x1 - x3 =< 3
		SDCLinearConstraint cons3 = new SDCLinearConstraint(var1,var3, 3, SDCLinearConstraint.LESS_EQUAL);
		// x3 - x1 =< -3
		SDCLinearConstraint cons4 = new SDCLinearConstraint(var3, var1, -3, SDCLinearConstraint.LESS_EQUAL);
		// x4 - x3 =< -1
		SDCLinearConstraint cons5 = new SDCLinearConstraint(var4, var3, -1, SDCLinearConstraint.LESS_EQUAL);
		// x5 - x4 =< 4
		SDCLinearConstraint cons6 = new SDCLinearConstraint(var5, var4, 4, SDCLinearConstraint.LESS_EQUAL);
		
		// Add constraints to the SDC
		sdc.addConstraint(cons1);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons3);
		sdc.addConstraint(cons4);
		sdc.addConstraint(cons5);
		sdc.addConstraint(cons6);
		
		// Set a feasibility checker
		sdc.setFeasibilityChecker(new BellmanFordChecker());
		// Check feasibility
		System.out.println("Is the system feasible? "+sdc.checkFeasibility());
		// Remove a constraint
		sdc.removeConstraint(cons4);
		// Again check the feasibility, SDC should still be feasible
		System.out.println("Is the system still feasible? "+sdc.checkFeasibility());
		// Construct and add new constraint
		SDCLinearConstraint consNew = new SDCLinearConstraint(var2, var1, -1, SDCLinearConstraint.LESS_EQUAL);
		sdc.addConstraint(consNew);
		// Again check the feasibility, SDC should still be feasible
		System.out.println("Is the system still feasible? "+sdc.checkFeasibility());
		// Update the right-hand side of an existing constraint
		sdc.updateConstraint(consNew, -2);
		// Again check the feasibility, SDC should now be infeasible
		System.out.println("Is the system infeasible now? "+sdc.checkFeasibility());

	}

}
