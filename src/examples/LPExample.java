/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package examples;

import framework.lp.LPProblemImpl;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.lp.solver.LPSolveSolver;
import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;

/**
 * Small example of a LP to demonstrate the use of the framework.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPExample {

	public static void main(String[] args) {
		// Instantiate new LP
		LPProblemImpl lp = new LPProblemImpl();
		// Restrict all variables to positive values
		lp.setRestrictedNonNegative(true);
		// Set solver
		LPSolver solver = new LPSolveSolver();
		lp.setSolver(solver);
		// Create new variables
		LPVariable x1 = new LPVariable("x1");
		LPVariable x2 = new LPVariable("x2");
		LPVariable x3 = new LPVariable("x3");

		// Construct and add constraints
		// 35 * x1 + 0.5 * x2 + 0.5 * x3 >= 0.5
		lp.addConstraint(new LPLinearConstraint(new double[] { 35, 0.5, 0.5 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 0.5));
		// 30 * x1 + 20 * x2 + 10 * x3 >= 4
		lp.addConstraint(new LPLinearConstraint(new double[] { 30, 20, 10 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 4));
		// 60 * x1 + 300 * x2 + 10 * x3 >= 15
		lp.addConstraint(new LPLinearConstraint(new double[] { 60, 300, 10 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 15));

		// Build and set objective function
		// MIN 0.75 * x1 + 0.5 * x2 + 0.15 * x3
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(x1, 0.75);
		objfun.addVarCoefficient(x2, 0.5);
		objfun.addVarCoefficient(x3, 0.15);
		lp.setObjectiveFunction(objfun);

		// Solve the LP
		lp.solve();

		// Print the optimal values for each variable
		// x1 approx. 0.0095
		System.out.println("x1: " + x1.getOptimalSolution());
		// x2 approx. 0.03
		System.out.println("x2: " + x2.getOptimalSolution());
		// x3 approx. 0.29
		System.out.println("x3: " + x3.getOptimalSolution());

		// Print the value of the objective function, approx. 0.07
		System.out.println("Objective function: " + solver.getValueObjectiveFunction());
	}

}
