/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.lp.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Set;

import framework.model.OptimVariable;

public class LPTestUtil {

	public static boolean checkForIntegralSolution(Set<? extends OptimVariable> variables) {
		boolean integral = true;
		for (OptimVariable var : variables) {
			integral &= var.hasIntegralOptimalSolution();
		}
		return integral;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value)) {
			return Double.NaN;
		}
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static double roundToSignificant(double value, int significantPlaces) {
		BigDecimal bd = new BigDecimal(value);
		bd = bd.round(new MathContext(significantPlaces));
		return bd.doubleValue();
	}

	public static boolean checkSolution(Map<? extends OptimVariable, Double> solution, boolean round) {
		boolean allCorrect = true;
		for (OptimVariable var : solution.keySet()) {
			double isValue = var.getOptimalSolution();
			if (round) {
				isValue = round(isValue, 3);
			}
			double desiredValue = solution.get(var);
			if (isValue != desiredValue) {
				System.out.println("Var " + var.getName() + " should be: " + desiredValue + " but was: " + isValue + " Error: " + Math.abs(desiredValue - isValue));
				allCorrect = false;
			}
		}
		return allCorrect;
	}

}
