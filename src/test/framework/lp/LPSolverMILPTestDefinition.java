/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.lp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import test.framework.lp.util.LPTestUtil;
import framework.lp.LPProblemImpl;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;

public abstract class LPSolverMILPTestDefinition {

	protected abstract LPSolver getLPSolver();
	
	@Test
	public void testILP(){
		LPSolver solver = getLPSolver();
		LPProblemImpl ilp = new LPProblemImpl();
		LPVariable x = new LPVariable("x");
		LPVariable y = new LPVariable("y");
		LPVariable [] vars = new LPVariable[]{x,y};
		LPLinearConstraint c1 = new LPLinearConstraint(new double[]{-1.0, 1.0}, vars, OptimLinearConstraint.LESS_EQUAL, 1);
		LPLinearConstraint c2 = new LPLinearConstraint(new double[]{3.0, 2.0}, vars, OptimLinearConstraint.LESS_EQUAL, 12);
		LPLinearConstraint c3 = new LPLinearConstraint(new double[]{2.0, 3.0}, vars, OptimLinearConstraint.LESS_EQUAL, 12);
		OptimLinearVariableConstraint xbound = new OptimLinearVariableConstraint(x, 0, Double.POSITIVE_INFINITY);
		OptimLinearVariableConstraint ybound = new OptimLinearVariableConstraint(y, 0, Double.POSITIVE_INFINITY);
		x.setOnlyInteger(true);
		y.setOnlyInteger(true);
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MAX);
		objfun.addVarCoefficient(x, 1);
		objfun.addVarCoefficient(y, 10);
		
		ilp.addConstraint(c1);
		ilp.addConstraint(c2);
		ilp.addConstraint(c3);
		ilp.addVariableConstraint(xbound);
		ilp.addVariableConstraint(ybound);
		ilp.setObjectiveFunction(objfun);
		ilp.setSolver(solver);
		
		ilp.solve();
		
		assertTrue(x.getOptimalSolution()==2);
		assertTrue(y.getOptimalSolution()==2);
		assertTrue(LPTestUtil.roundToSignificant(solver.getValueObjectiveFunction(), 3)==22);
	}

	@Test
	public void testBinaryILP(){
		LPSolver solver = getLPSolver();
		LPProblemImpl ilp = new LPProblemImpl();
		LPVariable x = new LPVariable("x");
		LPVariable y = new LPVariable("y");
		LPVariable [] vars = new LPVariable[]{x,y};
		LPLinearConstraint c1 = new LPLinearConstraint(new double[]{-1.0, 1.0}, vars, OptimLinearConstraint.LESS_EQUAL, 1);
		LPLinearConstraint c2 = new LPLinearConstraint(new double[]{3.0, 2.0}, vars, OptimLinearConstraint.LESS_EQUAL, 12);
		LPLinearConstraint c3 = new LPLinearConstraint(new double[]{2.0, 3.0}, vars, OptimLinearConstraint.LESS_EQUAL, 12);
		OptimLinearVariableConstraint ybound = new OptimLinearVariableConstraint(y, 0, Double.POSITIVE_INFINITY);
		x.setOnlyBinary(true);
		y.setOnlyInteger(true);
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MAX);
		objfun.addVarCoefficient(x, 1);
		objfun.addVarCoefficient(y, 10);
		
		ilp.addConstraint(c1);
		ilp.addConstraint(c2);
		ilp.addConstraint(c3);
		ilp.addVariableConstraint(ybound);
		ilp.setObjectiveFunction(objfun);
		ilp.setSolver(solver);
		
		ilp.solve();
		
		assertTrue(x.getOptimalSolution()==1);
		assertTrue(y.getOptimalSolution()==2);
		assertTrue(LPTestUtil.roundToSignificant(solver.getValueObjectiveFunction(), 3)==21);
	}
}
