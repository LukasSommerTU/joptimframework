/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.lp;

import org.junit.Test;

import framework.lp.solver.LPSolver;
import framework.lp.solver.WVLPSolver;

public class WVLPSolverTest extends LPSolverTestDefinition {

	@Override
	protected LPSolver getLPSolver() {
		return new WVLPSolver();
	}

	@Override
	@Test
	public void testNetworkFlow() {
		System.out.println("###------ Network Flow ------###");
		// LPProblemImpl lp = new LPProblemImpl();
		// LPSolver solver = getLPSolver();
		// lp.setSolver(solver);
		// Map<OptimVariable, Double> solution = new HashMap<OptimVariable,
		// Double>();
		// OptimVariable xoa = new OptimVariable("xoa");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xoa }, OptimLinearConstraint.GREATER_EQUAL, -3));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xoa }, OptimLinearConstraint.LESS_EQUAL, 3));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xoa, -3, 3));
		// solution.put(xoa, 2.);
		// OptimVariable xob = new OptimVariable("xob");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xob }, OptimLinearConstraint.GREATER_EQUAL, -1));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xob }, OptimLinearConstraint.LESS_EQUAL, 1));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xob, -1, 1));
		// solution.put(xob, 1.);
		// OptimVariable xoc = new OptimVariable("xoc");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xoc }, OptimLinearConstraint.GREATER_EQUAL, -1));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xoc }, OptimLinearConstraint.LESS_EQUAL, 1));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xoc, -1, 1));
		// solution.put(xoc, 1.);
		// OptimVariable xab = new OptimVariable("xab");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xab }, OptimLinearConstraint.GREATER_EQUAL, -1));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xab }, OptimLinearConstraint.LESS_EQUAL, 1));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xab, -1, 1));
		// solution.put(xab, 1.);
		// OptimVariable xad = new OptimVariable("xad");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xad }, OptimLinearConstraint.GREATER_EQUAL, -1));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xad }, OptimLinearConstraint.LESS_EQUAL, 1));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xad, -1, 1));
		// solution.put(xad, 1.);
		// OptimVariable xbe = new OptimVariable("xbe");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xbe }, OptimLinearConstraint.GREATER_EQUAL, -3));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xbe }, OptimLinearConstraint.LESS_EQUAL, 3));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xbe, -3, 3));
		// solution.put(xbe, 2.);
		// OptimVariable xcd = new OptimVariable("xcd");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xcd }, OptimLinearConstraint.GREATER_EQUAL, -4));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xcd }, OptimLinearConstraint.LESS_EQUAL, 4));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xcd, -4, 4));
		// solution.put(xcd, 2.);
		// OptimVariable xce = new OptimVariable("xce");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xce }, OptimLinearConstraint.GREATER_EQUAL, -4));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xce }, OptimLinearConstraint.LESS_EQUAL, 4));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xce, -4, 4));
		// solution.put(xce, -1.);
		// OptimVariable xdn = new OptimVariable("xdn");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xdn }, OptimLinearConstraint.GREATER_EQUAL, -4));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xdn }, OptimLinearConstraint.LESS_EQUAL, 4));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xdn, -4, 4));
		// solution.put(xdn, 3.);
		// OptimVariable xen = new OptimVariable("xen");
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xen }, OptimLinearConstraint.GREATER_EQUAL, -1));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1 }, new
		// OptimVariable[] { xen }, OptimLinearConstraint.LESS_EQUAL, 1));
		// // lp.addVariableConstraint(new OptimLinearVariableConstraint(xen, -1, 1));
		// solution.put(xen, 1.);
		//
		// lp.addConstraint(new LPLinearConstraint(new double[] { -1, -1, 1 },
		// new OptimVariable[] { xab, xad, xoa }, OptimLinearConstraint.EQUALS, 0));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 },
		// new OptimVariable[] { xob, xab, xbe }, OptimLinearConstraint.EQUALS, 0));
		// lp.addConstraint(new LPLinearConstraint(new double[] { -1, -1, 1 },
		// new OptimVariable[] { xcd, xce, xoc }, OptimLinearConstraint.EQUALS, 0));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 },
		// new OptimVariable[] { xad, xcd, xdn }, OptimLinearConstraint.EQUALS, 0));
		// lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 },
		// new OptimVariable[] { xbe, xce, xen }, OptimLinearConstraint.EQUALS, 0));
		//
		// lp.setRestrictedNonNegative(false);
		//
		// OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0,
		// OptimLinearObjectiveFunction.MAX);
		// objfun.addVarCoefficient(xoa, 1);
		// objfun.addVarCoefficient(xob, 1);
		// objfun.addVarCoefficient(xoc, 1);
		// objfun.addVarCoefficient(xen, 1);
		// lp.setObjectiveFunction(objfun);
		//
		// checkValidity(lp.solve(), null, "LP should be solved");
		// checkValidity(LPTestUtil.checkSolution(solution, false), null,
		// "LP solver should give correct values!");
		// double optVal = 5.0;
		// double compVal = solver.getValueObjectiveFunction();
		// if (!Double.isNaN(compVal)) {
		// checkValidity((compVal == optVal), "Correct objective value!",
		// "Optimal value of objective function should be " + optVal +
		// " but was : " + compVal);
		// }
	}
}
