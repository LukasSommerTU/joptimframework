/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.lp;

import framework.lp.solver.LPSolveSolver;
import framework.lp.solver.LPSolver;

/**
 * Test the lp_solve based solver. Some tests may fail due to slight imprecision
 * or because lp_solve found another optimal solution. Essential for the correct
 * functionality is the value of the objective function printed to the output.
 * 
 * @author Lukas Sommer <lukas.sommer.mail@gmail.com>
 *
 */
public class LPSolveSolverTest extends LPSolverTestDefinition {

	@Override
	protected LPSolver getLPSolver() {
		LPSolveSolver solver = new LPSolveSolver();
		solver.setPivotingRule(1);
		return solver;
	}

}
