/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.lp;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import test.framework.lp.util.LPTestUtil;
import framework.lp.LPProblemImpl;
import framework.lp.model.LPLinearConstraint;
import framework.lp.model.LPVariable;
import framework.lp.solver.LPSolver;
import framework.model.OptimLinearConstraint;
import framework.model.OptimLinearObjectiveFunction;
import framework.model.OptimLinearVariableConstraint;
import framework.sdc.SDC;
import framework.sdc.feasibility_checker.BellmanFordChecker;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCSingleVariableConstraint;
import framework.sdc.model.SDCVariable;

public abstract class LPSolverTestDefinition {

	protected abstract LPSolver getLPSolver();

	@Rule
	public ErrorCollector collector = new ErrorCollector();

	@Test
	public void testOptimizedDiet() {
		System.out.println("###------ Optimized Diet ------###");
		LPProblemImpl lp = new LPProblemImpl();
		lp.setRestrictedNonNegative(true);
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		LPVariable x1 = new LPVariable("x1");
		LPVariable x2 = new LPVariable("x2");
		LPVariable x3 = new LPVariable("x3");
		lp.addConstraint(new LPLinearConstraint(new double[] { 35, 0.5, 0.5 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 0.5));
		lp.addConstraint(new LPLinearConstraint(new double[] { 30, 20, 10 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 4));
		lp.addConstraint(new LPLinearConstraint(new double[] { 60, 300, 10 }, new LPVariable[] { x1, x2, x3 }, OptimLinearConstraint.GREATER_EQUAL, 15));
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(x1, 0.75);
		objfun.addVarCoefficient(x2, 0.5);
		objfun.addVarCoefficient(x3, 0.15);
		lp.setObjectiveFunction(objfun);
		checkValidity(lp.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.roundToSignificant(x1.getOptimalSolution(), 2) == 0.0095, null, "Rounded optimal value should be 0.0095");
		checkValidity(LPTestUtil.roundToSignificant(x2.getOptimalSolution(), 2) == 0.038, null, "Rounded optimal value should be 0.038");
		checkValidity(LPTestUtil.roundToSignificant(x3.getOptimalSolution(), 2) == 0.29, null, "Rounded optimal value should be 0.29");

		double optVal = 0.07;
		double compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 2);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
	}

	@Test
	public void testJOptimizerExample() {
		System.out.println("###------ JOptimizer Example ------###");
		LPProblemImpl lp = new LPProblemImpl();
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		LPVariable x1 = new LPVariable("x1");
		LPVariable x2 = new LPVariable("x2");
		LPVariable[] vars = new LPVariable[] { x1, x2 };
		lp.addConstraint(new LPLinearConstraint(new double[] { 4. / 3., -1 }, vars, OptimLinearConstraint.LESS_EQUAL, 2));
		lp.addConstraint(new LPLinearConstraint(new double[] { -1. / 2., 1. }, vars, OptimLinearConstraint.LESS_EQUAL, 0.5));
		lp.addConstraint(new LPLinearConstraint(new double[] { -2., -1. }, vars, OptimLinearConstraint.LESS_EQUAL, 2));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1. / 3., 1. }, vars, OptimLinearConstraint.LESS_EQUAL, 0.5));
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(x1, -1);
		objfun.addVarCoefficient(x2, -1);
		lp.setObjectiveFunction(objfun);
		checkValidity(lp.solve(), null, "LP should be solved");
		Map<LPVariable, Double> solution = new HashMap<LPVariable, Double>();
		solution.put(x1, 1.5);
		solution.put(x2, 0.0);
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");
		double optVal = -1.5;
		double compVal = LPTestUtil.roundToSignificant(solver.getValueObjectiveFunction(), 3);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
	}

	@Test
	public void testSimpleEquals() {
		System.out.println("###------ Simple Equals ------###");
		LPProblemImpl lp = new LPProblemImpl();
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		LPVariable x1 = new LPVariable("x1");
		LPVariable x2 = new LPVariable("x2");
		LPVariable[] vars = new LPVariable[] { x1, x2 };
		lp.addVariableConstraint(new OptimLinearVariableConstraint(x1, 0, 5));
		lp.addVariableConstraint(new OptimLinearVariableConstraint(x2, 0, 5));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, -1 }, vars, OptimLinearConstraint.EQUALS, 0));

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MAX);
		objfun.addVarCoefficient(x1, 1);
		objfun.addVarCoefficient(x2, 1);

		lp.setObjectiveFunction(objfun);

		checkValidity(lp.solve(), null, "LP should be solved");

		double optVal = 10.0;
		double compVal = solver.getValueObjectiveFunction();
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
	}

	@Test
	public void testNetworkFlow() {
		System.out.println("###------ Network Flow ------###");
		LPProblemImpl lp = new LPProblemImpl();
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		Map<LPVariable, Double> solution = new HashMap<LPVariable, Double>();
		LPVariable xoa = new LPVariable("xoa");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xoa, -3, 3));
		solution.put(xoa, 2.);
		LPVariable xob = new LPVariable("xob");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xob, -1, 1));
		solution.put(xob, 1.);
		LPVariable xoc = new LPVariable("xoc");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xoc, -1, 1));
		solution.put(xoc, 1.);
		LPVariable xab = new LPVariable("xab");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xab, -1, 1));
		solution.put(xab, 1.);
		LPVariable xad = new LPVariable("xad");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xad, -1, 1));
		solution.put(xad, 1.);
		LPVariable xbe = new LPVariable("xbe");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xbe, -3, 3));
		solution.put(xbe, 2.);
		LPVariable xcd = new LPVariable("xcd");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xcd, -4, 4));
		solution.put(xcd, 2.);
		LPVariable xce = new LPVariable("xce");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xce, -4, 4));
		solution.put(xce, -1.);
		LPVariable xdn = new LPVariable("xdn");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xdn, -4, 4));
		solution.put(xdn, 3.);
		LPVariable xen = new LPVariable("xen");
		lp.addVariableConstraint(new OptimLinearVariableConstraint(xen, -1, 1));
		solution.put(xen, 1.);

		lp.addConstraint(new LPLinearConstraint(new double[] { -1, -1, 1 }, new LPVariable[] { xab, xad, xoa }, OptimLinearConstraint.EQUALS, 0));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 }, new LPVariable[] { xob, xab, xbe }, OptimLinearConstraint.EQUALS, 0));
		lp.addConstraint(new LPLinearConstraint(new double[] { -1, -1, 1 }, new LPVariable[] { xcd, xce, xoc }, OptimLinearConstraint.EQUALS, 0));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 }, new LPVariable[] { xad, xcd, xdn }, OptimLinearConstraint.EQUALS, 0));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1, -1 }, new LPVariable[] { xbe, xce, xen }, OptimLinearConstraint.EQUALS, 0));

		lp.setRestrictedNonNegative(false);

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MAX);
		objfun.addVarCoefficient(xoa, 1);
		objfun.addVarCoefficient(xob, 1);
		objfun.addVarCoefficient(xoc, 1);
		objfun.addVarCoefficient(xen, 1);
		lp.setObjectiveFunction(objfun);

		checkValidity(lp.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, false), null, "LP solver should give correct values!");
		double optVal = 5.0;
		double compVal = solver.getValueObjectiveFunction();
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
	}

	@Test
	public void testSDC1() {
		System.out.println("###------ SDC Test 1 ------###");
		SDC sdc = new SDC();
		LPSolver solver = getLPSolver();
		sdc.setSolver(solver);
		sdc.setFeasibilityChecker(new BellmanFordChecker());
		SDCVariable x1 = new SDCVariable("x1");
		SDCVariable x2 = new SDCVariable("x2");
		SDCVariable x3 = new SDCVariable("x3");
		SDCVariable x4 = new SDCVariable("x4");
		SDCVariable x5 = new SDCVariable("x5");
		sdc.addConstraint(new SDCLinearConstraint(x1, x2, 0, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x1, x5, -1, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x2, x5, 1, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x3, x1, 5, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x4, x1, 4, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x4, x3, -1, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x5, x3, -3, OptimLinearConstraint.LESS_EQUAL));
		sdc.addConstraint(new SDCLinearConstraint(x5, x4, -3, OptimLinearConstraint.LESS_EQUAL));
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(x1, 1);
		objfun.addVarCoefficient(x2, 1);
		objfun.addVarCoefficient(x3, 1);
		objfun.addVarCoefficient(x4, 1);
		objfun.addVarCoefficient(x5, 1);
		sdc.setObjectiveFunction(objfun);
		checkValidity(sdc.checkFeasibility(), null, "SDC infeasible");
		checkValidity(sdc.solve(), null, "SDC should be solved");
		Map<SDCVariable, Double> solution = new HashMap<SDCVariable, Double>();
		solution.put(x1, 0.0);
		solution.put(x2, 0.0);
		solution.put(x3, 5.0);
		solution.put(x4, 4.0);
		solution.put(x5, 1.0);
		checkValidity(LPTestUtil.checkSolution(solution, false), null, "LP solver should give correct values!");
		checkValidity(LPTestUtil.checkForIntegralSolution(solution.keySet()), null, "Solution of an SDC should be an integral!");
		assertTrue("Solution of an SDC should be integral!", LPTestUtil.checkForIntegralSolution(solution.keySet()));

		double optVal = 10.0;
		double compVal = solver.getValueObjectiveFunction();
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

	}

	@Test
	public void testSDC2() {
		System.out.println("###------ SDC Test 2 ------###");
		SDC sdc = new SDC();
		LPSolver solver = getLPSolver();
		sdc.setSolver(solver);
		sdc.setFeasibilityChecker(new BellmanFordChecker());
		SDCVariable x1 = new SDCVariable("x1");
		SDCVariable x2 = new SDCVariable("x2");
		SDCVariable x3 = new SDCVariable("x3");
		// x2 =< x1 + 5 <=> x2 - x1 =< 5
		SDCLinearConstraint cons1 = new SDCLinearConstraint(x2, x1, 5, OptimLinearConstraint.LESS_EQUAL);
		// x3 =< x1 - 3 <=> x3 - x1 =< -3
		SDCLinearConstraint cons2 = new SDCLinearConstraint(x3, x1, -3, OptimLinearConstraint.LESS_EQUAL);
		// x1 = 7
		SDCSingleVariableConstraint cons3 = new SDCSingleVariableConstraint(x1, 7, OptimLinearConstraint.EQUALS, sdc);
		sdc.addConstraint(cons1);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons3);
		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MAX);
		objfun.addVarCoefficient(x1, 1);
		objfun.addVarCoefficient(x2, 1);
		objfun.addVarCoefficient(x3, 1);
		sdc.setObjectiveFunction(objfun);
		checkValidity(sdc.checkFeasibility(), null, "SDC infeasible");
		checkValidity(sdc.solve(), null, "SDC should be solved");
		Map<SDCVariable, Double> solution = new HashMap<SDCVariable, Double>();
		solution.put(x1, 7.0);
		solution.put(x2, 12.0);
		solution.put(x3, 4.0);
		checkValidity(LPTestUtil.checkSolution(solution, false), null, "LP solver should give correct values!");
		checkValidity(LPTestUtil.checkForIntegralSolution(solution.keySet()), null, "Solution of an SDC should be an integral!");
		assertTrue("Solution of an SDC should be integral!", LPTestUtil.checkForIntegralSolution(solution.keySet()));

		double optVal = 23.0;
		double compVal = solver.getValueObjectiveFunction();
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

	}

	@Test
	public void test1988Example() {
		System.out.println("###------ 1988 UG Exam ------###");
		LPProblemImpl lp = new LPProblemImpl();
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		Map<LPVariable, Double> solution = new HashMap<LPVariable, Double>();
		LPVariable a = new LPVariable("a");
		solution.put(a, 8.);
		LPVariable b = new LPVariable("b");
		solution.put(b, 3.);
		LPVariable[] vars = new LPVariable[] { a, b };
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, 1 }, vars, OptimLinearConstraint.GREATER_EQUAL, 11));
		lp.addConstraint(new LPLinearConstraint(new double[] { 1, -1 }, vars, OptimLinearConstraint.LESS_EQUAL, 5));
		lp.addConstraint(new LPLinearConstraint(new double[] { 7, 12 }, vars, OptimLinearConstraint.GREATER_EQUAL, 35));
		lp.setRestrictedNonNegative(true);

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(a, 10);
		objfun.addVarCoefficient(b, 11);
		lp.setObjectiveFunction(objfun);

		checkValidity(lp.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		double optVal = 113.0;
		double compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
	}

	@Test
	public void updateLPExample() {
		System.out.println("###------ Update LP Example ------###");
		LPProblemImpl lp = new LPProblemImpl();
		LPSolver solver = getLPSolver();
		lp.setSolver(solver);
		Map<LPVariable, Double> solution = new HashMap<LPVariable, Double>();
		LPVariable a = new LPVariable("a");
		solution.put(a, 0.);
		LPVariable b = new LPVariable("b");
		solution.put(b, 3.);
		LPVariable c = new LPVariable("c");
		solution.put(c, 0.);
		LPVariable[] vars = new LPVariable[] { a, b, c };
		lp.addConstraint(new LPLinearConstraint(new double[] { -1, 1, 0 }, vars, OptimLinearConstraint.GREATER_EQUAL, 3));
		lp.setRestrictedNonNegative(true);

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(a, 1);
		objfun.addVarCoefficient(b, 1);
		objfun.addVarCoefficient(c, 1);
		lp.setObjectiveFunction(objfun);

		checkValidity(lp.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		double optVal = 3.0;
		double compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

		// Update the LP, solution must be invalidated and computed again
		lp.addConstraint(new LPLinearConstraint(new double[] { -1, 0, 1 }, vars, OptimLinearConstraint.GREATER_EQUAL, 5));
		solution.put(c, 5.);
		checkValidity(lp.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		optVal = 8.0;
		compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

	}

	@Test
	public void updateSDCExample() {
		System.out.println("###------ Update SDC Example ------###");
		SDC sdc = new SDC();
		LPSolver solver = getLPSolver();
		sdc.setSolver(solver);
		Map<SDCVariable, Double> solution = new HashMap<SDCVariable, Double>();
		SDCVariable a = new SDCVariable("a");
		solution.put(a, 0.);
		SDCVariable b = new SDCVariable("b");
		solution.put(b, 3.);
		SDCVariable c = new SDCVariable("c");
		solution.put(c, 0.);
		sdc.addConstraint(new SDCLinearConstraint(b, a, 3, OptimLinearConstraint.GREATER_EQUAL));

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(a, 1);
		objfun.addVarCoefficient(b, 1);
		objfun.addVarCoefficient(c, 1);
		sdc.setObjectiveFunction(objfun);

		checkValidity(sdc.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		double optVal = 3.0;
		double compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

		// Update the LP, solution must be invalidated and computed again
		sdc.addConstraint(new SDCLinearConstraint(c, a, 5, OptimLinearConstraint.GREATER_EQUAL));
		solution.put(c, 5.);
		checkValidity(sdc.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		optVal = 8.0;
		compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

	}
	
	@Test
	public void updateSingleVariableConstraintExample() {
		System.out.println("###------ Update Single Variable Constraint Example ------###");
		SDC sdc = new SDC();
		LPSolver solver = getLPSolver();
		sdc.setSolver(solver);
		Map<SDCVariable, Double> solution = new HashMap<SDCVariable, Double>();
		SDCVariable a = new SDCVariable("a");
		solution.put(a, 0.);
		SDCVariable b = new SDCVariable("b");
		solution.put(b, 3.);
		sdc.addConstraint(new SDCLinearConstraint(b, a, 3, OptimLinearConstraint.GREATER_EQUAL));

		OptimLinearObjectiveFunction objfun = new OptimLinearObjectiveFunction(0, OptimLinearObjectiveFunction.MIN);
		objfun.addVarCoefficient(a, 1);
		objfun.addVarCoefficient(b, 1);
		sdc.setObjectiveFunction(objfun);

		checkValidity(sdc.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");

		double optVal = 3.0;
		double compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

		// Update the LP, solution must be invalidated and computed again
		sdc.addConstraint(new SDCSingleVariableConstraint(a, 5, OptimLinearConstraint.GREATER_EQUAL, sdc));
		solution.put(b, 8.0);
		solution.put(a, 5.0);
		checkValidity(sdc.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");
		
		optVal = 13.0;
		compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}
		
		sdc.addConstraint(new SDCSingleVariableConstraint(a, 7, OptimLinearConstraint.GREATER_EQUAL, sdc));
		solution.put(b, 10.0);
		solution.put(a, 7.0);
		checkValidity(sdc.solve(), null, "LP should be solved");
		checkValidity(LPTestUtil.checkSolution(solution, true), null, "LP solver should give correct values!");
		
		optVal = 17.0;
		compVal = LPTestUtil.round(solver.getValueObjectiveFunction(), 5);
		if (!Double.isNaN(compVal)) {
			checkValidity((compVal == optVal), "Correct objective value!", "Optimal value of objective function should be " + optVal + " but was : " + compVal);
		}

	}

	public void checkValidity(boolean check, String successMessage, String errMessage) {
		try {
			assertTrue(check);
			if (successMessage != null) {
				System.out.println(successMessage);
			}
		} catch (Throwable e) {
			System.out.println(errMessage);
			collector.addError(e);
		}
	}
}
