package test.framework.lp.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.framework.lp.ApacheSimplexSolverTest;
import test.framework.lp.LPSolveSolverMILPTest;
import test.framework.lp.LPSolveSolverTest;
import test.framework.lp.WVLPSolverTest;

@RunWith(Suite.class)
@SuiteClasses({ LPSolveSolverMILPTest.class, LPSolveSolverTest.class, ApacheSimplexSolverTest.class, WVLPSolverTest.class })
public class SolverTestSuite {

}
