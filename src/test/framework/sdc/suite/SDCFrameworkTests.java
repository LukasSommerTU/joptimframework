/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.framework.sdc.BellmanFordCheckerTest;
import test.framework.sdc.IncrementalCheckerTest;
import test.framework.sdc.PairTest;
import test.framework.sdc.SDCLinearConstraintTest;
import test.framework.sdc.SDCTest;
import test.framework.sdc.SDCVariableTest;

@RunWith(Suite.class)
@SuiteClasses({ SDCLinearConstraintTest.class, SDCTest.class, SDCVariableTest.class, BellmanFordCheckerTest.class, IncrementalCheckerTest.class, PairTest.class })
public class SDCFrameworkTests {

}
