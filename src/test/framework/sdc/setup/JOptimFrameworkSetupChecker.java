/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc.setup;

import java.util.ArrayList;
import java.util.Collection;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.LinalgFactory;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.ColtMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;
import com.winvector.lp.LPSoln;
import com.winvector.lp.impl.RevisedSimplexSolver;

public class JOptimFrameworkSetupChecker {

	private static boolean failed = false;

	public static void testJOptimFrameworkSetup() {
		testApacheSimplexSolver1();
		testApacheSimplexSolver2();
		testWVLPSolver();
		testWVLPSolver2();
		System.out.println("Java library-path is: " + System.getProperty("java.library.path"));
		testLpSolveSolver();
		if (failed) {
			System.err.println("Not all solvers are set up correctly, check output for more details!");
		} else {
			System.out.println("Successfully checked framework setup!");
		}
	}

	@SuppressWarnings("unused")
	public static void testApacheSimplexSolver1() {
		// describe the optimization problem
		LinearObjectiveFunction f = new LinearObjectiveFunction(new double[] { -2, 1 }, -5);
		Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();
		constraints.add(new LinearConstraint(new double[] { 1, 2 }, Relationship.LEQ, 6));
		constraints.add(new LinearConstraint(new double[] { 3, 2 }, Relationship.LEQ, 12));
		constraints.add(new LinearConstraint(new double[] { 0, 1 }, Relationship.GEQ, 0));
		LinearConstraintSet cons = new LinearConstraintSet(constraints);
		try {
			// create and run the solver
			PointValuePair solution = new SimplexSolver().optimize(f, cons, GoalType.MINIMIZE);
			// get the solution
			double x = solution.getPoint()[0];
			double y = solution.getPoint()[1];
			double min = solution.getValue();
		} catch (Exception e) {
			System.err.println("Apache SimplexSolver is not set up correctly");
			failed = true;
		}
	}

	@SuppressWarnings("unused")
	public static void testApacheSimplexSolver2() {
		LinearObjectiveFunction f = new LinearObjectiveFunction(new double[] { 1, 1, 1, 1, 1 }, 0);
		double[][] G = new double[][] { { 1, -1, 0, 0, 0 }, { 1, 0, 0, 0, -1 }, { 0, 1, 0, 0, -1 }, { -1, 0, 1, 0, 0 }, { -1, 0, 0, 1, 0 }, { 0, 0, -1, 1, 0 }, { 0, 0, -1, 0, 1 }, { 0, 0, 0, -1, 1 } };
		double[] h = new double[] { 0, -1, 1, 5, 4, -1, -3, -3 };

		Collection<LinearConstraint> constr = new ArrayList<LinearConstraint>();
		for (int i = 0; i < G.length; ++i) {
			constr.add(new LinearConstraint(G[i], Relationship.LEQ, h[i]));
		}

		for (int i = 0; i < 5; ++i) {
			double[] coeff = new double[5];
			coeff[i] = 1;
			constr.add(new LinearConstraint(coeff, Relationship.GEQ, 0));
			constr.add(new LinearConstraint(coeff, Relationship.LEQ, Double.MAX_VALUE));
			coeff[i] = 0;
		}

		LinearConstraintSet constraints = new LinearConstraintSet(constr);
		try {
			PointValuePair solution = new SimplexSolver().optimize(f, constraints, GoalType.MINIMIZE);
		} catch (Exception e) {
			System.err.println("Apache SimplexSolver is not set up correctly");
			failed = true;
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public static <Z extends Matrix<Z>> void testWVLPSolver() {
		LinalgFactory<Z> factory = (LinalgFactory<Z>) ColtMatrix.factory;
		final Matrix<Z> m = factory.newMatrix(3, 5, false);
		final double[] b = new double[3];
		final double[] c = new double[5];
		m.set(0, 0, 1.0);
		m.set(0, 1, 1.0);
		m.set(0, 2, -1.0);
		b[0] = 4.0; // x1 + x2 - s1 = 4
		m.set(1, 0, 1.0);
		m.set(1, 1, 3.0);
		m.set(1, 3, -1.0);
		b[1] = 12.0; // x1 + 3*x2 - s2 = 12
		m.set(2, 0, 1.0);
		m.set(2, 1, -1.0);
		m.set(2, 4, -1.0); // x1 - x2 - s3 = 0
		c[0] = 2.0;
		c[1] = 1.0; // minimize 2*x1 + x2
		LPEQProb prob;
		try {
			prob = new LPEQProb(m.columnMatrix(), b, new DenseVec(c));
			final LPSoln soln1 = prob.solveDebug(new RevisedSimplexSolver(), 1.0e-6, 1000, factory);
		} catch (LPMalformedException e) {
			System.err.println("WVLPSolver is not set up corret");
			failed = true;
		} catch (LPException e) {
			System.err.println("WVLPSolver is not set up corret");
			failed = true;
		}

		final double[] expect = { 3.00000, 3.00000, 2.00000, 0.00000, 0.00000 };
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public static <Z extends Matrix<Z>> void testWVLPSolver2() {
		LinalgFactory<Z> factory = (LinalgFactory<Z>) ColtMatrix.factory;
		final Matrix<Z> m = factory.newMatrix(8, 13, false);
		final double[] b = new double[8];
		final double[] c = new double[13];
		m.set(0, 0, 1.0);
		m.set(0, 1, -1.0);
		m.set(0, 5, 1);
		b[0] = 0;
		m.set(1, 0, 1.0);
		m.set(1, 4, -1);
		m.set(1, 6, 1);
		b[1] = -1;
		m.set(2, 1, 1.0);
		m.set(2, 4, -1.0);
		m.set(2, 7, 1);
		b[2] = 1;
		m.set(3, 2, 1);
		m.set(3, 0, -1);
		m.set(3, 8, 1);
		b[3] = 5;
		m.set(4, 3, 1);
		m.set(4, 0, -1);
		m.set(4, 9, 1);
		b[4] = 4;
		m.set(5, 3, 1);
		m.set(5, 2, -1);
		m.set(5, 10, 1);
		b[5] = -1;
		m.set(6, 4, 1);
		m.set(6, 2, -1);
		m.set(6, 11, 1);
		b[6] = -3;
		m.set(7, 4, 1);
		m.set(7, 3, -1);
		m.set(7, 12, 1);
		b[7] = -3;
		c[0] = 2.0;
		c[1] = 1.0;
		c[2] = 1;
		c[3] = 1;
		c[4] = 1;
		LPEQProb prob;
		try {
			prob = new LPEQProb(m.columnMatrix(), b, new DenseVec(c));
			final LPSoln soln1 = prob.solveDebug(new RevisedSimplexSolver(), 1.0e-6, 1000, factory);
		} catch (LPMalformedException e) {
			System.err.println("WVLPSolver is not set up corret");
			failed = true;
		} catch (LPException e) {
			System.err.println("WVLPSolver is not set up corret");
			failed = true;
		}

	}

	public static void testLpSolveSolver() {
		try {
			// Create a problem with 4 variables and 0 constraints
			LpSolve solver = LpSolve.makeLp(0, 4);
			// add constraints
			solver.strAddConstraint("3 2 2 1", LpSolve.LE, 4);
			solver.strAddConstraint("0 4 3 1", LpSolve.GE, 3);
			solver.setUpbo(1, 17);
			// set objective function
			solver.strSetObjFn("2 3 -2 3");
			solver.setVerbose(0);
			// solver.printLp();
			// solve the problem
			solver.solve();
			// print solution
			// System.out.println("Value of objective function: " +
			// solver.getObjective());
			// double[] var = solver.getPtrVariables();
			// for (int i = 0; i < var.length; i++) {
			// System.out.println("Value of var[" + i + "] = " + var[i]);
			// }

			// delete the problem and free memory
			solver.deleteLp();
		} catch (LpSolveException e) {
			e.printStackTrace();
			failed = true;
		} catch (UnsatisfiedLinkError le) {
			le.printStackTrace();
			System.err.println("lp_solve is not setup correct");
			failed = true;
		}
	}
}
