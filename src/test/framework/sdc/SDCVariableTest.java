/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc;

import static org.junit.Assert.*;

import org.junit.Test;

import framework.sdc.model.SDCVariable;

public class SDCVariableTest {

	@Test
	public void testSDCVariable(){
		SDCVariable var1 = new SDCVariable("var1");
		assertTrue("Name of var1 should be correct", var1.getName().equals("var1"));
		SDCVariable var2 = new SDCVariable();
		assertTrue("Name of var2 should be empty", var2.getName() == "");
	}
	
	@Test
	public void testFeasibleSolution(){
		SDCVariable var = new SDCVariable();
		var.setFeasibleSolution(42);
		assertTrue("Feasible solution should be 42", var.getFeasibleSolution()==42);
		assertTrue("Feasible solution should be integral", var.hasIntegralFeasibleSolution());
		var.setFeasibleSolution(42.3);
		assertFalse("Feasible solution should not be integral", var.hasIntegralFeasibleSolution());
		var.setFeasibleSolution(42.5);
		assertFalse("Feasible solution should not be integral", var.hasIntegralFeasibleSolution());
		var.setFeasibleSolution(42.7);
		assertFalse("Feasible solution should not be integral", var.hasIntegralFeasibleSolution());
	}
	
	@Test
	public void testOptimalSolution(){
		SDCVariable var = new SDCVariable();
		var.setOptimalSolution(42);
		assertTrue("Optimal solution should be 42", var.getOptimalSolution()==42);
		assertTrue("Optimal solution should be integral", var.hasIntegralOptimalSolution());
		var.setOptimalSolution(42.3);
		assertFalse("Optimal solution should not be integral", var.hasIntegralOptimalSolution());
		var.setOptimalSolution(42.5);
		assertFalse("Optimal solution should not be integral", var.hasIntegralOptimalSolution());
		var.setOptimalSolution(42.7);
		assertFalse("Optimal solution should not be integral", var.hasIntegralOptimalSolution());
	}

}
