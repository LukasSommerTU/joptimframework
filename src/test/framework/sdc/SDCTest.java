/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import framework.sdc.SDC;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;

public class SDCTest {
	
	SDC sdc = new SDC();
	SDCVariable var1 = new SDCVariable("x1");
	SDCVariable var2 = new SDCVariable("x2");
	SDCLinearConstraint cons1 = new SDCLinearConstraint(var1, var2, 5.0, SDCLinearConstraint.LESS_EQUAL);
	SDCLinearConstraint cons2 = new SDCLinearConstraint(var2, var1, 5.0, SDCLinearConstraint.EQUALS);
	Set<SDCLinearConstraint> constraints = new HashSet<SDCLinearConstraint>();
	Set<SDCVariable> variables = new HashSet<SDCVariable>();
	@Test
	public void testSDCTrivially() {
		assertTrue("Trivial SDC should be feasible!", sdc.checkFeasibility());
	}
	
	@Test
	public void testAddConstraint(){
		sdc.addConstraint(cons1);
		constraints = sdc.getConstraints();
		assertTrue("SDC should contain one constraint",(constraints.size()==1));
		assertTrue("Constraint1 should be contained in the SDC", constraints.contains(cons1));
		variables = sdc.getVariables();
		assertTrue("SDC should contain 2 variables", (variables.size() == 2));
		assertTrue("Var1 should be contained in the SDC", variables.contains(var1));
		assertTrue("Var2 should be contained in the SDC", variables.contains(var2));
		assertFalse("Non-trivial SDC should not be feasible without feasibility-checker", sdc.checkFeasibility());
		sdc.addConstraint(cons2);
		constraints = sdc.getConstraints();
		assertTrue("SDC should contain 2 constraints",(constraints.size()==2));
		assertTrue("Constraint1 should still be contained in the SDC", constraints.contains(cons1));
		assertTrue("Constraint2 should be contained in the SDC", constraints.contains(cons2));
		variables = sdc.getVariables();
		assertTrue("SDC should still contain 2 variables", (variables.size() == 2));
		assertFalse("Non-trivial SDC should not be feasible without feasibility-checker", sdc.checkFeasibility());
	}
	
	@Test
	public void testUpdateConstraint(){
		sdc.addConstraint(cons1);
		assertTrue("Return value should be true", sdc.updateConstraint(cons1, 42));
		SDCLinearConstraint updatedCons = sdc.getConstraints().iterator().next();
		assertTrue("Constraint constant value should be updated", (updatedCons.getRHS()==42));
		assertTrue("Constraint1 should have been updated", (cons1.getRHS()==42));
		assertTrue("Variable1 should not have been updated", (updatedCons.getVariable1()==cons1.getVariable1() && cons1.getVariable1() == var1));
		assertTrue("Variable2 should not have been updated", (updatedCons.getVariable2()==cons1.getVariable2() && cons1.getVariable2() == var2));
		assertTrue("Type should not have been updated", (updatedCons.getType() == cons1.getType() && cons1.getType()==SDCLinearConstraint.LESS_EQUAL));
		assertFalse("Return value should be false", sdc.updateConstraint(cons2, 3.141));
	}
	
	@Test
	public void testRemoveConstraint(){
		sdc.addConstraint(cons1);
		assertTrue("Return value should be true",sdc.removeConstraint(cons1));
		assertTrue("SDC should contain no constraint", sdc.getConstraints().isEmpty());
		assertFalse("Cons1 should not be contained any more", sdc.getConstraints().contains(cons1));
		assertFalse("Return value should be false",sdc.removeConstraint(cons2));
	}
}
