/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import framework.model.OptimLinearConstraint;
import framework.sdc.SDC;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;

public abstract class FeasibilityCheckerTestDefinition {

	protected SDC sdc;
	private SDCVariable var1;
	private SDCVariable var2;
	private SDCVariable var3;
	private SDCVariable var4;
	private SDCVariable var5;

	private SDCLinearConstraint cons1;
	private SDCLinearConstraint cons2;
	private SDCLinearConstraint cons3;
	private SDCLinearConstraint cons4;
	private SDCLinearConstraint cons5;
	private SDCLinearConstraint cons6;

	private SDCVariable x1;
	private SDCVariable x2;
	private SDCVariable x3;
	private SDCVariable x4;

	@Before
	public void doSetup() {
		sdc = new SDC();
		setFeasibilityCheckerUnderTest();
	}

	private void constructExample1SDC() {
		var1 = new SDCVariable("x1");
		var2 = new SDCVariable("x2");
		var3 = new SDCVariable("x3");
		var4 = new SDCVariable("x4");
		var5 = new SDCVariable("x5");
		cons1 = new SDCLinearConstraint(var1, var2, 3, SDCLinearConstraint.LESS_EQUAL);
		cons2 = new SDCLinearConstraint(var3, var2, -2, SDCLinearConstraint.LESS_EQUAL);
		cons3 = new SDCLinearConstraint(var1, var3, 3, SDCLinearConstraint.LESS_EQUAL);
		cons4 = new SDCLinearConstraint(var3, var1, -3, SDCLinearConstraint.LESS_EQUAL);
		cons5 = new SDCLinearConstraint(var4, var3, -1, SDCLinearConstraint.LESS_EQUAL);
		cons6 = new SDCLinearConstraint(var5, var4, 4, SDCLinearConstraint.LESS_EQUAL);

		sdc.addConstraint(cons1);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons3);
		sdc.addConstraint(cons4);
		sdc.addConstraint(cons5);
		sdc.addConstraint(cons6);
	}

	private void constructExample2SDC() {
		var1 = new SDCVariable("x1");
		var2 = new SDCVariable("x2");
		var3 = new SDCVariable("x3");
		cons1 = new SDCLinearConstraint(var2, var1, 3, SDCLinearConstraint.LESS_EQUAL);
		cons2 = new SDCLinearConstraint(var3, var2, 3, SDCLinearConstraint.LESS_EQUAL);
		cons3 = new SDCLinearConstraint(var1, var3, -3, SDCLinearConstraint.LESS_EQUAL);

		sdc.addConstraint(cons1);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons3);
	}

	private void constructCycleExampleSDC() {
		x1 = new SDCVariable("x1");
		x2 = new SDCVariable("x2");
		x3 = new SDCVariable("x3");
		x4 = new SDCVariable("x4");

		SDCLinearConstraint c1 = new SDCLinearConstraint(x2, x1, -3, OptimLinearConstraint.LESS_EQUAL);
		SDCLinearConstraint c2 = new SDCLinearConstraint(x3, x4, 0, OptimLinearConstraint.EQUALS);
		sdc.addConstraint(c1);
		sdc.addConstraint(c2);
	}

	private void constructSingleVariableExample() {
		x1 = new SDCVariable("x1");
		x2 = new SDCVariable("x2");
		x3 = new SDCVariable("x3");
		// x2 >= x1 + 5 <=> x2 - x1 >= 5
		cons1 = new SDCLinearConstraint(x2, x1, 5, OptimLinearConstraint.GREATER_EQUAL);
		// x3 =< x1 - 3 <=> x3 - x1 =< -3
		cons2 = new SDCLinearConstraint(x3, x1, -3, OptimLinearConstraint.LESS_EQUAL);
		// x1 = 7
		SDCVariable dv = sdc.getDummyVar();
		cons3 = new SDCLinearConstraint(x1, dv, 7, OptimLinearConstraint.EQUALS);
		sdc.addConstraint(cons1);
		sdc.addConstraint(cons2);
		sdc.addConstraint(cons3);
	}

	protected abstract void setFeasibilityCheckerUnderTest();

	@Test
	public void testExample1() {
		constructExample1SDC();
		assertTrue("System is feasibleSolution", sdc.checkFeasibility());
		sdc.removeConstraint(cons4);
		assertTrue("Removing a constraint doesn't make the system infeasible", sdc.checkFeasibility());
		SDCLinearConstraint consNew = new SDCLinearConstraint(var2, var1, -1, SDCLinearConstraint.LESS_EQUAL);
		sdc.addConstraint(consNew);
		assertTrue("Adding this constraint doesn't make the system infeasible", sdc.checkFeasibility());
		sdc.updateConstraint(consNew, -2);
		assertFalse("The new constraint should make the system infeasible", sdc.checkFeasibility());
	}

	@Test
	public void testExample2() {
		constructExample2SDC();
		assertTrue("System is feasibleSolution", sdc.checkFeasibility());
	}

	@Test
	public void testCycleExample() {
		constructCycleExampleSDC();
		assertTrue("System should be feasible!", sdc.checkFeasibility());
		SDCLinearConstraint c3 = new SDCLinearConstraint(x3, x2, 0, OptimLinearConstraint.LESS_EQUAL);
		sdc.addConstraint(c3);
	}

	@Test
	public void testSingleVariableExample() {
		constructSingleVariableExample();
		assertTrue("System should be feasible!", sdc.checkFeasibility());
		assertTrue("Wrong value for variable!", (x1.getFeasibleSolution() == 7));
		assertTrue("Wrong value for variable!", (x2.getFeasibleSolution() >= 12));
		assertTrue("Wrong value for variable!", (x3.getFeasibleSolution() <= 4));
		SDCVariable dv = sdc.getDummyVar();
		cons4 = new SDCLinearConstraint(x3, dv, 3, OptimLinearConstraint.LESS_EQUAL);
		sdc.addConstraint(cons4);
		assertTrue("System should be feasible!", sdc.checkFeasibility());
		assertTrue("Wrong value for variable!", (x3.getFeasibleSolution() <= 3));
		cons5 = new SDCLinearConstraint(x2, dv, 11, OptimLinearConstraint.EQUALS);
		sdc.addConstraint(cons5);
		assertFalse("System should be infeasible!", sdc.checkFeasibility());
		sdc.removeConstraint(cons5);
		assertTrue("System should be feasible!", sdc.checkFeasibility());
		assertTrue("Wrong value for variable!", (x1.getFeasibleSolution() == 7));
		assertTrue("Wrong value for variable!", (x2.getFeasibleSolution() >= 12));
		assertTrue("Wrong value for variable!", (x3.getFeasibleSolution() <= 3));
		cons6 = new SDCLinearConstraint(x1, dv, 8, OptimLinearConstraint.EQUALS);
		sdc.addConstraint(cons6);
		assertFalse("System should be infeasible!", sdc.checkFeasibility());
	}
}
