package test.framework.sdc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import framework.sdc.feasibility_checker.model.util.Pair;

public class PairTest {

	@Test
	public void testEquals() {
		Pair<Integer, Integer> pair1 = new Pair<Integer, Integer>(1, 2);
		Pair<Integer, Integer> pair2 = new Pair<Integer, Integer>(1, 2);
		Pair<Integer, Integer> pair3 = new Pair<Integer, Integer>(2, 1);
		assertTrue("Pair1 should equal pair2", pair1.equals(pair2));
		assertTrue("Pair2 should equal pair1", pair2.equals(pair1));
		assertFalse("Pair1 should not equal pair3", pair1.equals(pair3));
		assertFalse("Pair3 should not equal pair1", pair3.equals(pair1));
	}

	@Test
	public void testHashCode() {
		Pair<Integer, Integer> pair1 = new Pair<Integer, Integer>(1, 2);
		Pair<Integer, Integer> pair2 = new Pair<Integer, Integer>(1, 2);
		Pair<Integer, Integer> pair3 = new Pair<Integer, Integer>(2, 1);
		assertTrue("Pair1's hash code should equal pair2's hash code", pair1.hashCode() == pair2.hashCode());
		assertFalse("Pair1's hash code should not equal pair3s hash code", pair1.hashCode() == pair3.hashCode());
	}
}
