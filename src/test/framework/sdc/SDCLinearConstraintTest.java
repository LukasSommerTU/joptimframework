/*******************************************************************************
 * Copyright 2014 Lukas Sommer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package test.framework.sdc;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import framework.sdc.feasibility_checker.model.SDCFeasibilityGraphEdge;
import framework.sdc.model.SDCLinearConstraint;
import framework.sdc.model.SDCVariable;

public class SDCLinearConstraintTest {
	
	SDCVariable var1 = new SDCVariable("x1");
	SDCVariable var2 = new SDCVariable("x2");

	@Test
	public void testEdgeRepresentation1() {
		SDCLinearConstraint cons1 = new SDCLinearConstraint(var1, var2, 5.0, SDCLinearConstraint.LESS_EQUAL);
		SDCLinearConstraint cons2 = new SDCLinearConstraint(var1, var2, 5.0, SDCLinearConstraint.EQUALS);
		assertTrue("Constraint1 should be represented by one edge", cons1.getEdgeRepresentation().size()==1);
		SDCFeasibilityGraphEdge cons1edge1 = cons1.getEdgeRepresentation().iterator().next();
		assertTrue("Starting point should be var2", cons1edge1.getEdgeStart()==var2);
		assertTrue("End point should be var1", cons1edge1.getEdgeEnd()==var1);
		assertTrue("Weight should be 5", cons1edge1.getWeight()==5);
		
		assertTrue("Constraint2 should be represented by 2 edge", cons2.getEdgeRepresentation().size()==2);
		Iterator<SDCFeasibilityGraphEdge> edgeIterator = cons2.getEdgeRepresentation().iterator();
		SDCFeasibilityGraphEdge cons2edge1 = edgeIterator.next();
		SDCFeasibilityGraphEdge cons2edge2 = edgeIterator.next();
		if(cons2edge1.getEdgeStart()==var2){
			assertTrue("Starting point should be var2", cons2edge1.getEdgeStart()==var2);
			assertTrue("End point should be var1", cons2edge1.getEdgeEnd()==var1);
			assertTrue("Weight should be 5", cons2edge1.getWeight()==5);
			assertTrue("Starting point should be var1", cons2edge2.getEdgeStart()==var1);
			assertTrue("End point should be var2", cons2edge2.getEdgeEnd()==var2);
			assertTrue("Weight should be -5", cons2edge2.getWeight()==-5);
		}
		else if(cons2edge1.getEdgeStart() == var1){
			assertTrue("Starting point should be var1", cons2edge1.getEdgeStart()==var1);
			assertTrue("End point should be var2", cons2edge1.getEdgeEnd()==var2);
			assertTrue("Weight should be -5", cons2edge1.getWeight()==-5);
			assertTrue("Starting point should be var2", cons2edge2.getEdgeStart()==var2);
			assertTrue("End point should be var1", cons2edge2.getEdgeEnd()==var1);
			assertTrue("Weight should be 5", cons2edge2.getWeight()==5);
		}
	}
	
	@Test
	public void testEdgeRepresentation2() {
		SDCLinearConstraint cons1 = new SDCLinearConstraint(var2, var1, 5.0, SDCLinearConstraint.LESS_EQUAL);
		SDCLinearConstraint cons2 = new SDCLinearConstraint(var2, var1, 5.0, SDCLinearConstraint.EQUALS);
		assertTrue("Constraint1 should be represented by one edge", cons1.getEdgeRepresentation().size()==1);
		SDCFeasibilityGraphEdge cons1edge1 = cons1.getEdgeRepresentation().iterator().next();
		assertTrue("Starting point should be var1", cons1edge1.getEdgeStart()==var1);
		assertTrue("End point should be var2", cons1edge1.getEdgeEnd()==var2);
		assertTrue("Weight should be 5", cons1edge1.getWeight()==5);
		
		assertTrue("Constraint2 should be represented by 2 edges", cons2.getEdgeRepresentation().size()==2);
		Iterator<SDCFeasibilityGraphEdge> edgeIterator = cons2.getEdgeRepresentation().iterator();
		SDCFeasibilityGraphEdge cons2edge1 = edgeIterator.next();
		SDCFeasibilityGraphEdge cons2edge2 = edgeIterator.next();
		if(cons2edge1.getEdgeStart()==var2){
			assertTrue("Starting point should be var2", cons2edge1.getEdgeStart()==var2);
			assertTrue("End point should be var1", cons2edge1.getEdgeEnd()==var1);
			assertTrue("Weight should be 5", cons2edge1.getWeight()==-5);
			assertTrue("Starting point should be var1", cons2edge2.getEdgeStart()==var1);
			assertTrue("End point should be var2", cons2edge2.getEdgeEnd()==var2);
			assertTrue("Weight should be -5", cons2edge2.getWeight()==5);
		}
		else if(cons2edge1.getEdgeStart() == var1){
			assertTrue("Starting point should be var1", cons2edge1.getEdgeStart()==var1);
			assertTrue("End point should be var2", cons2edge1.getEdgeEnd()==var2);
			assertTrue("Weight should be -5", cons2edge1.getWeight()==5);
			assertTrue("Starting point should be var2", cons2edge2.getEdgeStart()==var2);
			assertTrue("End point should be var1", cons2edge2.getEdgeEnd()==var1);
			assertTrue("Weight should be 5", cons2edge2.getWeight()==-5);
		}
	}
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

//	@Test
//	public void testImpossibleEdgeRepresentation() throws RuntimeException{
//		thrown.expect(RuntimeException.class);
//		thrown.expectMessage("Cannot create edge from this constraint!");
//		SDCLinearConstraint cons1 = new SDCLinearConstraint(1,var1, 1, var2, 5.0, SDCLinearConstraint.LESS_EQUAL);
//		cons1.getEdgeRepresentation();
//	}
	
	@Test
	public void testSimilarityTest(){
		SDCLinearConstraint cons1 = new SDCLinearConstraint(var2, var1, 5.0, SDCLinearConstraint.LESS_EQUAL);
		SDCLinearConstraint cons2 = new SDCLinearConstraint(var1, var2, 5.0, SDCLinearConstraint.EQUALS);
		assertTrue("Constraints with different coefficients are not similar", (cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_NEW));
		cons1 = new SDCLinearConstraint(var1,var2, 5.0, SDCLinearConstraint.EQUALS);
		assertTrue("Constraints should be similar", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_OLD);
		cons1.setRHS(42);
		assertTrue("Two equalities with different constants should not be similar", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.INFEASIBLE);
		cons1 = new SDCLinearConstraint(var1,var2, 5.0, SDCLinearConstraint.LESS_EQUAL);
		cons2 = new SDCLinearConstraint(var1,var2, 42, SDCLinearConstraint.LESS_EQUAL);
		assertTrue("Smaller constraint should win", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_OLD);
		cons2.setRHS(-42);
		assertTrue("Smaller constraint should win", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_NEW);
		
		cons1 = new SDCLinearConstraint(var2, var1, 42, SDCLinearConstraint.EQUALS);
		cons2 = new SDCLinearConstraint(var2, var1, 5, SDCLinearConstraint.LESS_EQUAL);
		assertTrue("System is infeasible", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.INFEASIBLE);
		cons1.setRHS(-42);
		assertTrue("Constraint1 is more restrictive", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_OLD);
		
		cons1 = new SDCLinearConstraint(var2, var1, 5, SDCLinearConstraint.LESS_EQUAL);
		cons2 = new SDCLinearConstraint(var2, var1, 42, SDCLinearConstraint.EQUALS);
		assertTrue("System is infeasible", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.INFEASIBLE);
		cons2.setRHS(-42);
		assertTrue("Constraint2 is more restrictive", cons1.isSimilarConstraint(cons2)==SDCLinearConstraint.KEEP_NEW);
	}

}
