package test.framework;

import static org.junit.Assert.*;

import org.junit.Test;

import framework.lp.model.LPLinearConstraint;
import framework.model.OptimLinearConstraint;
import framework.model.OptimVariable;

public class LinearConstraintCompareTest {
	
	@Test
	public void testLE_LE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint le = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint le1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 3.0);
		LPLinearConstraint le2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 42.0);
		assertTrue("New constraint should be kept", le.compareConstraints(le1)==OptimLinearConstraint.KEEP_NEW);
		assertTrue("Old constraint should be kept", le.compareConstraints(le2)==OptimLinearConstraint.KEEP_OLD);
	}
	
	@Test
	public void testLE_GE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint le = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 42.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 3.0);
		assertTrue("Should be infeasibe!", le.compareConstraints(ge1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("Both constraints should be kept", le.compareConstraints(ge2)==OptimLinearConstraint.KEEP_BOTH);
	}

	@Test
	public void testLE_EQ(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint le = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint eq1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 42.0);
		LPLinearConstraint eq2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 3.0);
		assertTrue("Should be infeasibe!", le.compareConstraints(eq1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("New constraint should be kept", le.compareConstraints(eq2)==OptimLinearConstraint.KEEP_NEW);
	}
	
	@Test
	public void testEQ_EQ(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint eq = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 5.0);
		LPLinearConstraint eq1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 5.0);
		LPLinearConstraint eq2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 42.0);
		assertTrue("Old constraint should be kept", eq.compareConstraints(eq1)==OptimLinearConstraint.KEEP_OLD);
		assertTrue("Should be infeasible", eq.compareConstraints(eq2)==OptimLinearConstraint.INFEASIBLE);
	}
	
	@Test
	public void testEQ_LE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint eq = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 5.0);
		LPLinearConstraint le1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 42.0);
		LPLinearConstraint le2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 3.0);
		assertTrue("Old constraint should be kept", eq.compareConstraints(le1)==OptimLinearConstraint.KEEP_OLD);
		assertTrue("Should be infeasibe!", eq.compareConstraints(le2)==OptimLinearConstraint.INFEASIBLE);
	}
	
	@Test
	public void testEQ_GE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint eq = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 3.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 42.0);
		assertTrue("New constraint should be kept", eq.compareConstraints(ge1)==OptimLinearConstraint.KEEP_OLD);
		assertTrue("Should be infeasibe!", eq.compareConstraints(ge2)==OptimLinearConstraint.INFEASIBLE);
	}
	
	@Test
	public void testGE_GE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint ge = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 42.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 3.0);
		assertTrue("New constraint should be kept", ge.compareConstraints(ge1)==OptimLinearConstraint.KEEP_NEW);
		assertTrue("Old constraint should be kept", ge.compareConstraints(ge2)==OptimLinearConstraint.KEEP_OLD);
	}
	
	@Test
	public void testGE_LE(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint ge = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 5.0);
		LPLinearConstraint le1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 3.0);
		LPLinearConstraint le2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 42.0);
		assertTrue("Should be infeasibe!", ge.compareConstraints(le1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("Both constraints should be kept", ge.compareConstraints(le2)==OptimLinearConstraint.KEEP_BOTH);
	}
	
	public void testGE_EQ(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint ge = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 5.0);
		LPLinearConstraint eq1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 3.0);
		LPLinearConstraint eq2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.EQUALS, 42.0);
		assertTrue("Should be infeasibe!", ge.compareConstraints(eq1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("New constraint should be kept", ge.compareConstraints(eq2)==OptimLinearConstraint.KEEP_NEW);
	}
	
	@Test
	public void testINV(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		LPLinearConstraint le = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{-5.0,-3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, -42.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{-5.0,-3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, -3.0);
		assertTrue("Should be infeasibe!", le.compareConstraints(ge1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("Both constraints should be kept", le.compareConstraints(ge2)==OptimLinearConstraint.KEEP_BOTH);
	}
	
	@Test
	public void testZeroCoeff(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		OptimVariable var3 = new OptimVariable("var3");
		LPLinearConstraint le1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint le2 = new LPLinearConstraint(new double[]{5.0,3.0, 0.0},new OptimVariable[]{var1, var2, var3},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{5.0,3.0,0.0},new OptimVariable[]{var1, var2,var3},OptimLinearConstraint.GREATER_EQUAL, 42.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 3.0);
		assertTrue("Should be infeasibe!", le1.compareConstraints(ge1)==OptimLinearConstraint.INFEASIBLE);
		assertTrue("Both constraints should be kept", le2.compareConstraints(ge2)==OptimLinearConstraint.KEEP_BOTH);
	}
	
	@Test
	public void testSimple(){
		OptimVariable var1 = new OptimVariable("var1");
		OptimVariable var2 = new OptimVariable("var2");
		OptimVariable var3 = new OptimVariable("var3");
		LPLinearConstraint le1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint le2 = new LPLinearConstraint(new double[]{5.0,4.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.LESS_EQUAL, 5.0);
		LPLinearConstraint ge1 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var3},OptimLinearConstraint.GREATER_EQUAL, 42.0);
		LPLinearConstraint ge2 = new LPLinearConstraint(new double[]{5.0,3.0},new OptimVariable[]{var1, var2},OptimLinearConstraint.GREATER_EQUAL, 3.0);
		assertTrue("Both constriants should be kept", le1.compareConstraints(ge1)==OptimLinearConstraint.KEEP_BOTH);
		assertTrue("Both constraints should be kept", le2.compareConstraints(ge2)==OptimLinearConstraint.KEEP_BOTH);
	}
}
